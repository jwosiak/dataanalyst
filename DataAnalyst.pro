TEMPLATE = subdirs

SUBDIRS += \
    ModelsModule \
    UtilitiesModule \
    DataProviderModule \
    UserInterfaceModule

CONFIG += c++11


copydata.commands = $(COPY_DIR) $$PWD/UtilitiesModule/Utilities/Resources/config.json $$OUT_PWD
first.depends = $(first) copydata
export(first.depends)
export(copydata.commands)
QMAKE_EXTRA_TARGETS += first copydata
