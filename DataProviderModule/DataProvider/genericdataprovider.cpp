/****************************************************************************
**
** Copyright (C) 2018  Jakub Wosiak
** Contact: jakubwosiak@gmail.com
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#include "genericdataprovider.h"
#include <continuousdatatype.h>
#include <datatype.h>
#include <stringtype.h>
#include <genericfactory.h>

#include <QJsonArray>
#include <QJsonObject>
#include <QSqlRecord>
#include <QSqlQuery>
#include <QSqlField>
#include <QListWidget>
#include <QtGlobal>

#include <QtDebug>
#include <iostream>
#include <QSqlError>
#include <QTime>


static ConfigReader *conf = ConfigReader::getInstance();
static GenericFactory *factory = GenericFactory::getInstance();

GenericDataProvider::GenericDataProvider()
{

}

GenericDataProvider *GenericDataProvider::getInstance()
{
    static GenericDataProvider *instance = new GenericDataProvider();

    return instance;
}

ElementsList GenericDataProvider::selectElements(const QStringList &filters,
                                                 const QString &orderBy,
                                                 const QString &sortOrder,
                                                 int offset, int limit,
                                                 int category)
{
    ElementsList result;
    QJsonObject cat = conf->chartCategoriesArray().at(category).toObject();
    QJsonObject search = conf->value(cat, "search").toObject();
    QJsonValue queryPattern = conf->value(search, "query");

    bool readQueryFailed = queryPattern.isUndefined() || queryPattern.isNull();
    if (!checkDatabase() || readQueryFailed)
    {
        if (readQueryFailed)
        {
            qDebug() << "Data Provider error: Failed to read query pattern from categories->search->query";
        }
        return result;
    }

    QPair<QStringList, AliasMap> columnsAndAliases =
            columnsNamesAliases(category);

    QJsonArray idsArray = conf->value(cat, "passed ids").toArray();
    QMap<QString, bool> ids = conf->passedIds(idsArray);
    AliasMap idsAliases = conf->passedIdsAliases(idsArray);

    QString whereCondition = filters.join(" AND ");
    QString order = orderBy + " " + sortOrder;
    whereCondition = whereCondition.length() == 0? "true" : whereCondition;

    QString filledQuery =
            queryPattern.toString()
            .arg((columnsAndAliases.first + ids.keys()).join(", "))
            .arg(whereCondition)
            .arg(order)
            .append(QString(" offset %1 limit %2").arg(offset).arg(limit));

    QSqlQuery executedQuery(filledQuery, *this->database);
    executedQuery.setForwardOnly(true);
    executedQuery.execBatch();

    if (executedQuery.lastError().isValid())
    {
        qDebug() << "Data Provider error: Encountered SQL error during fetching data:"
                 << executedQuery.lastError().text();
        return result;
    }

    while (executedQuery.next())
    {
        QSqlRecord record = executedQuery.record();
        Record *r = qSqlRecordToMap(record);

        QPair<QString, QString> element;
        element.first =
                recordToString(record, columnsAndAliases.first.length());
        element.second = constructSetPointer(ids, idsAliases, r);

        result.second << element;
        delete r;
    }

    result.first =
            transformRawColumnsNames(columnsAndAliases.first,
                                     columnsAndAliases.second).join(" | ");
    return result;
}

QPair<QStringList, AliasMap> GenericDataProvider::columnsNamesAliases(int category)
{
    AliasMap nameMap;
    QStringList columns;
    QJsonArray columnsConfig =
            conf->value(conf->searchConfig(category), "columns").toArray();

    for (int i = 0; i < columnsConfig.count(); i++)
    {
        QJsonObject mapping = columnsConfig.at(i).toObject();
        QString column = conf->value(mapping, "raw").toString();
        nameMap.insert(column,
                       conf->value(mapping, "displayed").toString());
        columns << column;
    }

    return QPair<QStringList, AliasMap>(columns, nameMap);
}

QStringList GenericDataProvider::transformRawColumnsNames(const QStringList &columns,
                                                          AliasMap nameMap)
{
    QStringList result;

    for (const QString &s : columns)
    {
        QString alias = nameMap.value(s);
        if (!alias.isEmpty())
        {
            result << alias;
        }
    }

    return result;
}

int GenericDataProvider::selectElementsCount(const QStringList &filters,
                                             int category)
{
    QJsonObject search = conf->searchConfig(category);
    QJsonValue queryPattern = conf->value(search, "query");

    bool readQueryFailed = queryPattern.isUndefined() || queryPattern.isNull();
    if (!checkDatabase() || readQueryFailed)
    {
        if (readQueryFailed)
        {
            qDebug() << "Data Provider error: Failed to read query pattern from categories->search->query";
        }
        return 0;
    }

    QString whereCondition = filters.join(" AND ");
    QString filledQuery = QString("%1 %2 %3")
            .arg("select count(*) from (")
            .arg((queryPattern.toString()
                 .arg("0 AS \"a\"")
                 .arg(whereCondition.length() == 0? "true" : whereCondition))
                 .arg("a"))
            .arg(") q2");

    QSqlQuery executedQuery = this->database->exec(filledQuery);

    if (executedQuery.next())
    {
        return executedQuery.value(0).toInt();
    }

    return 0;
}

QList<DataRecord *> GenericDataProvider::selectData(const QString &setPointer,
                                                    const QString &recordName,
                                                    const QList<AliasMap> *onlyColumns)
{
    QList<DataRecord *> result;

    QJsonObject bindingConf = conf->chartsConfig();
    QList<QMap<QString, QString> > axesConfigs;

    if (onlyColumns == nullptr)
    {
        axesConfigs << conf->typesConfig(bindingConf, "axes")
                    << conf->typesConfig(bindingConf, "grouped by");
    }
    else
    {
        axesConfigs << *onlyColumns;
    }

    QJsonValue queryPattern = conf->value(bindingConf, "query");

    bool readQueryFailed = queryPattern.isUndefined() || queryPattern.isNull();
    if (!checkDatabase() || readQueryFailed)
    {
        if (readQueryFailed)
        {
            qDebug() << "Data Provider error: Failed to read query pattern from categories->items->query";
        }
        return result;
    }

    const QString wholeQueryPattern = "WITH %1 AS (%2) SELECT %3 ;";

    QPair<QString, QStringList> fieldConfig =
            tupleFields("field_%1", axesConfigs,
                        conf->selectGroupedBy());

    const QString auxQueryName = "auxQuery_1";

    QString filledQuery = wholeQueryPattern
            .arg(auxQueryName)
            .arg(queryPattern.toString()
                .arg(fieldConfig.first)
                .arg(setPointer))
            .arg(constructArrayTuple(fieldConfig.second, auxQueryName));

    QList<QSqlRecord> records;

    QSqlQuery executedQuery(*this->database);
    executedQuery.setForwardOnly(true);
    executedQuery.exec(filledQuery);

    if (executedQuery.next())
    {
        int i = 0;
        for (const QMap<QString, QString> &axisConfig : axesConfigs)
        {
            QString wholeRawColumn = executedQuery.value(i).toString();
            QStringList wholeColumn = rawCells(wholeRawColumn);

            if (result.isEmpty())
            {
                const QString recordNameLabel = "Nazwa elementu";
                for (int j = 0; j < wholeColumn.length(); j++)
                {
                    DataRecord *r = new DataRecord();
                    r->insert(recordNameLabel, new StringType(recordName));
                    result.append(r);
                }

            }

            const QString label = axisConfig.value("label");
            const DataTypeCons cons =
                    factory->getCons(axisConfig.value("type"));
            QList<DataRecord *>::iterator iter = result.begin();
            for (QString &rawCell : wholeColumn)
            {
                (*iter)->insert(label, cons(QVariant(rawCell)));
                iter++;
            }
            i++;
        }
    }

    if (executedQuery.lastError().isValid())
    {
        qDebug() << "Data Provider error: Encountered SQL error during fetching data:"
                 << executedQuery.lastError().text();
    }

    return result;
}

QPair<QString, QStringList> GenericDataProvider::tupleFields(const QString &fieldAliasPattern,
                                                             const QList<QMap<QString, QString> > &axesConfig,
                                                             const QList<Record *> &groupedBy)
{
    QStringList fieldsAliases;
    QStringList fieldsNames;

    const QString column = "column";
    const QString fieldNamePattern = "%1 AS \"%2\"";
    int i = 0;

    for (const QMap<QString, QString> &axisConfig : axesConfig)
    {
        const QString fieldAlias = QString(fieldAliasPattern).arg(i++);
        fieldsNames << fieldNamePattern
                       .arg(axisConfig.value(column))
                       .arg(fieldAlias);

        fieldsAliases << fieldAlias;
    }

    for (Record *r : groupedBy)
    {
        const QString fieldAlias = QString(fieldAliasPattern).arg(i++);
        fieldsNames << fieldNamePattern
                       .arg(r->value(column).toString())
                       .arg(fieldAlias);

        fieldsAliases << fieldAlias;
    }

    return QPair<QString, QStringList>(fieldsNames.join(", "), fieldsAliases);
}

GroupedValues GenericDataProvider::selectColumnForGen(int category,
                                                      QString column)
{
    GroupedValues result;
    QString columnType = conf->axisType(column);
    QString columnName = conf->axisColumn(column);
    DataTypeCons cons = factory->getCons(columnType);
    QJsonObject genConfig = conf->attribGen();
    AliasMap sourceIds = conf->passedIdsAliases(
                conf->value(conf->chartCategoriesArray().at(category).toObject(),
                            "passed ids").toArray());

    QJsonArray destIdsArray = conf->value(genConfig, "passed ids").toArray();
    QMap<QString, bool> destIds = conf->passedIds(destIdsArray);

    QList<AliasMap> neededColumns;
    AliasMap nextColumn;
    nextColumn.insert("column", columnName);
    nextColumn.insert("type", columnType);
    nextColumn.insert("label", column);

    neededColumns << nextColumn;
    nextColumn.clear();
    for (const QString &id : sourceIds.values() + destIds.keys())
    {
        nextColumn.insert("column", id);
        nextColumn.insert("type", "string");
        nextColumn.insert("label", id);
        neededColumns << nextColumn;
        nextColumn.clear();
    }

    QList<DataRecord *> data = selectData("true", "", &neededColumns);

    QList<DataRecord *>::iterator iter = data.begin();
    for (; iter != data.end(); iter++)
    {
        DataRecord *r = *iter;
        QStringList keyParts;
        QString key, valuePointer;
        ContinuousDataType *nextValue =
                static_cast<ContinuousDataType *>(r->value(column));
        QStringList ids;

        for (const QString &id : sourceIds.values())
        {
            keyParts << r->value(id)->toString();
        }

        key = keyParts.join(" | ");

        for (int i = 0; i < destIdsArray.count(); i++)
        {
            QJsonObject obj = destIdsArray.at(i).toObject();
            QString col = conf->value(obj, "column").toString();
            ids << r->value(col)->toString();
        }

        GroupedValues::iterator innerIter = result.find(key);

        if (innerIter == result.end())
        {
            QList<ValueWithIds> *group = new QList<ValueWithIds>();
            innerIter = result.insert(key, group);
        }

        (*innerIter)->append(ValueWithIds(nextValue, ids));
        delete r;
    }
    data.clear();
    return result;
}

QString GenericDataProvider::recordToString(QSqlRecord &r, int limit)
{
    QStringList result;

    for (int i = 0; i < r.count() && i < limit; i++)
    {
        result << r.value(i).toString();
    }

    return result.join(" | ");
}

QVariant GenericDataProvider::recordToVariant(Record *r)
{
    return qVariantFromValue(static_cast<void*>(r));
}

Record *GenericDataProvider::recordFromVariant(QVariant *v)
{
    return static_cast<Record *>(v->value<void*>());
}

DataType *GenericDataProvider::dataFromVariant(QVariant *v)
{
    return static_cast<DataType *>(v->value<void*>());
}

QListWidgetItem *GenericDataProvider::packRecord(QString label, Record *r)
{

    QListWidgetItem *item = new QListWidgetItem();
    item->setText(label);
    item->setData(Qt::UserRole, recordToVariant(r));

    return item;
}

Record *GenericDataProvider::unpackRecord(QListWidgetItem *item)
{
    return static_cast<Record*>(item->data(Qt::UserRole).value<void*>());
}

QList<ContinuousDataRecord *> GenericDataProvider::plainToContinuousData(const QList<DataRecord *> plainData)
{
    QList<ContinuousDataRecord *> result;
    for (DataRecord *r : plainData)
    {
        ContinuousDataRecord *cr = new ContinuousDataRecord();

        for (const QString &key : r->keys())
        {
            cr->insert(key, dynamic_cast<ContinuousDataType *>(r->value(key)));
        }

        result.append(cr);
    }

    return result;
}

QString GenericDataProvider::constructArrayTuple(const QStringList &auxQueryFields,
                                                 const QString &auxQueryName)
{
    QStringList result;
    const QString column = "column";
    for (const QString &fieldName : auxQueryFields)
    {
        result << QString("ARRAY(SELECT ARRAY[%1] FROM %2)")
                  .arg(fieldName).arg(auxQueryName);
    }

    return result.join(", ");
}

QString GenericDataProvider::constructSetPointer(const QMap<QString, bool> &passedIds,
                                                 const AliasMap &idsAliases,
                                                 Record *record)
{
    QStringList result;
    const QString textPattern = "%1 = '%2'", numericPattern = "%1 = %2";

    for (const QString &id : passedIds.keys())
    {
        bool isTextType = passedIds.value(id);
        const QString column = idsAliases.value(id);
        const QString value = record->value(id).toString();
        if (isTextType)
        {
            result << textPattern.arg(column).arg(value);
        }
        else
        {
            result << numericPattern.arg(column).arg(value);
        }
    }
    return result.join(" AND ");
}

QStringList GenericDataProvider::rawCells(const QString &rawColumn)
{
    QStringList result = rawColumn.split("},{");
    result.first().remove("{{");
    result.last().remove("}}");
    return result;
}

bool GenericDataProvider::checkDatabase()
{
    if (!this->database->isOpen())
    {
        qDebug() << "Data Provider error: Database is not opened";
        return false;
    }
    return true;
}
