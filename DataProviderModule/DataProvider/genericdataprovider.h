/****************************************************************************
**
** Copyright (C) 2018  Jakub Wosiak
** Contact: jakubwosiak@gmail.com
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#ifndef GENERICDATAPROVIDER_H
#define GENERICDATAPROVIDER_H

#include "sqlcommunicator.h"
#include <QList>
#include <configreader.h>

class QListWidgetItem;
typedef QPair<ContinuousDataType *, QStringList> ValueWithIds;
typedef QMap<QString, QList<ValueWithIds> *> GroupedValues;
typedef QPair<QString, QList<QPair<QString, QString> > > ElementsList;

class GenericDataProvider : public SqlCommunicator
{
private:
    GenericDataProvider();
public:
    static GenericDataProvider *getInstance();

    ElementsList selectElements(const QStringList &filters,
                                const QString &orderBy, const QString &sortOrder,
                                int offset, int limit, int category);

    QPair<QStringList, AliasMap> columnsNamesAliases(int category);
    QStringList transformRawColumnsNames(const QStringList &columns,
                                         AliasMap nameMap);

    int selectElementsCount(const QStringList &filters, int category);

    QList<DataRecord*> selectData(const QString &setPointer,
                                  const QString &recordName,
                                  const QList<AliasMap> *onlyColumns = nullptr);

    QPair<QString, QStringList> tupleFields(const QString &fieldAliasPattern,
                                            const QList<QMap<QString, QString>> &axesConfig,
                                            const QList<Record *> &groupedBy);

    GroupedValues selectColumnForGen(int category, QString column);

    QString recordToString(QSqlRecord &r, int limit);
    QVariant recordToVariant(Record *r);
    Record *recordFromVariant(QVariant *v);
    DataType *dataFromVariant(QVariant *v);

    QListWidgetItem *packRecord(QString label, Record *r);
    Record *unpackRecord(QListWidgetItem *item);

    QList<ContinuousDataRecord *> plainToContinuousData(const QList<DataRecord *> plainData);

    QString constructArrayTuple(const QStringList &auxQueryFields,
                                const QString &auxQueryName);
    QString constructSetPointer(const QMap<QString, bool> &passedIds,
                                const AliasMap &idsAliases,
                                Record *record);

    QStringList rawCells(const QString &rawColumn);
private:
    bool checkDatabase();
};

#endif // GENERICDATAPROVIDER_H
