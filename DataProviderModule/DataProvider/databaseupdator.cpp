/****************************************************************************
**
** Copyright (C) 2018  Jakub Wosiak
** Contact: jakubwosiak@gmail.com
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#include "databaseupdator.h"

#include <QSqlQuery>
#include <QSqlError>
#include <QtDebug>
#include <QtGlobal>
#include <ctime>
#include <configreader.h>
#include <continuousdatatype.h>
#include <QJsonArray>
#include <QJsonObject>

static ConfigReader *conf = ConfigReader::getInstance();

DatabaseUpdator::DatabaseUpdator()
{

}

DatabaseUpdator *DatabaseUpdator::getInstance()
{
    static DatabaseUpdator *instance = new DatabaseUpdator();

    return instance;
}

QString DatabaseUpdator::realColumnParams(QString columnName)
{
    return QString("%1 REAL").arg(columnName);
}

QString DatabaseUpdator::addColumnAndTempTable(QString tableName, QString columnParams)
{
    QString tmpName = randomTableName(20);

    const QString filledQuery =
            QString("ALTER table %1 ADD COLUMN IF NOT EXISTS %2; CREATE TEMP TABLE %3 AS SELECT * FROM %1 LIMIT 0; CREATE INDEX %3_ind ON %3(%4)")
            .arg(tableName).arg(columnParams).arg(tmpName)
            .arg(conf->itemsPassedIds().join(", "));
    this->database->exec(filledQuery);

    if (this->database->lastError().isValid())
    {
        qDebug() << "Attribute Generator error: Encountered SQL error during creating new column and a temporary table:"
                 << this->database->lastError();
    }
    return tmpName;
}

void DatabaseUpdator::insertIntoTempTable(QString tableName, QString column,
                                          GroupedValues &ivs, int offset,
                                          int limit)
{
    QStringList wholeQuery;

    QStringList columns = conf->itemsPassedIds();
    QJsonArray idsArr = conf->attribGen().value("passed ids").toArray();

    const QString insertQueryPattern =
            QString("INSERT INTO %1 (%2, %3) VALUES %4")
            .arg(tableName).arg(columns.join(", ")).arg(column);
    const QString singleTuplePattern = "(%1, %2)";

    int i = 0;
    for (QList<ValueWithIds> *iv : ivs.values())
    {
        if (i < limit+offset && i >= offset)
        {
            for (ValueWithIds &v : *iv)
            {
                for (int j = 0; j < idsArr.count(); j++)
                {
                    if (idsArr.at(j).toObject().value("text type").toBool())
                    {
                        v.second[j] = QString("'%1'").arg(v.second[j]);
                    }
                }
                wholeQuery << QString(singleTuplePattern)
                              .arg(v.second.join(", ")).arg(v.first->toReal());
            }
        }

        i++;
    }


    QString filledQuery =
            QString(insertQueryPattern).arg(wholeQuery.join(','));

    this->database->exec(filledQuery);

    if (this->database->lastError().isValid())
    {
        qDebug() << "Attribute Generator error: Encountered SQL error during inserting data to a temporary table:"
                 << this->database->lastError();
    }
}

void DatabaseUpdator::updateColumn(QString tableName, QString column,
                                   QString tmpTableName)
{
    const QString keyCompPattern = "%1.%2";
    QStringList comparisons1, comparisons2;

    for (const QString &s : conf->itemsPassedIds())
    {

        comparisons1 << keyCompPattern
                       .arg(tableName)
                       .arg(s);
        comparisons2 << keyCompPattern
                       .arg(tmpTableName)
                       .arg(s);
    }


    const QString filledQuery =
            QString("UPDATE %1 SET %2 = %3.%2 FROM %3 WHERE (%4) = (%5); TRUNCATE TABLE %3;")
            .arg(tableName)
            .arg(column)
            .arg(tmpTableName)
            .arg(comparisons1.join(", "))
            .arg(comparisons2.join(", "));

    this->database->exec(filledQuery);


    if (this->database->lastError().isValid())
    {
        qDebug() << "Attribute Generator error: Encountered SQL error during inserting data from a temporary table to new column:"
                 << this->database->lastError();
    }
}

void DatabaseUpdator::updateNullRows(QString tableName, QString column,
                                     QString defaultValue)
{
    const QString filledQuery =
            QString("UPDATE %1 SET %2 = %3 WHERE %2 IS NULL;")
            .arg(tableName).arg(column).arg(defaultValue);

    this->database->exec(filledQuery);

    if (this->database->lastError().isValid())
    {
        qDebug() << "Attribute Generator error: Encountered SQL error during filling null rows with default values:"
                 << this->database->lastError();
    }
}

QString DatabaseUpdator::randomTableName(int saltLength)
{
    QStringList salt;
    qsrand(uint(time(nullptr)));
    while (saltLength-- > 0)
    {
        salt << QString("%1").arg(qrand() % 10);
    }

    return "tmp" + salt.join("");
}
