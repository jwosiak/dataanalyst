/****************************************************************************
**
** Copyright (C) 2018  Jakub Wosiak
** Contact: jakubwosiak@gmail.com
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#ifndef DATABASEUPDATOR_H
#define DATABASEUPDATOR_H

#include "genericdataprovider.h"

class DatabaseUpdator : public SqlCommunicator
{
private:
    DatabaseUpdator();
public:
    static DatabaseUpdator *getInstance();

    QString realColumnParams(QString columnName);
    QString addColumnAndTempTable(QString tableName, QString columnParams);

    void insertIntoTempTable(QString tableName, QString column,
                             GroupedValues &ivs, int offset, int limit);

    void updateColumn(QString tableName, QString column, QString tmpTableName);

    void updateNullRows(QString tableName, QString column,
                        QString defaultValue);
private:
    QString randomTableName(int saltLength);
};

#endif // DATABASEUPDATOR_H
