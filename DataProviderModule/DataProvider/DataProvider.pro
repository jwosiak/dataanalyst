#-------------------------------------------------
#
# Project created by QtCreator 2017-04-28T22:44:16
#
#-------------------------------------------------

QT += sql
QT += widgets
QT -= gui

TARGET = DataProvider
TEMPLATE = lib
CONFIG += staticlib

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    sqlcommunicator.cpp \
    genericdataprovider.cpp \
    databaseupdator.cpp

HEADERS += \
    sqlcommunicator.h \
    genericdataprovider.h \
    databaseupdator.h
unix {
    target.path = /usr/lib
    INSTALLS += target
}

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../../ModelsModule/Models/release/ -lModels
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../../ModelsModule/Models/debug/ -lModels
else:unix: LIBS += -L$$OUT_PWD/../../ModelsModule/Models/ -lModels

INCLUDEPATH += $$PWD/../../ModelsModule/Models
DEPENDPATH += $$PWD/../../ModelsModule/Models

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../ModelsModule/Models/release/libModels.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../ModelsModule/Models/debug/libModels.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../ModelsModule/Models/release/Models.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../ModelsModule/Models/debug/Models.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../../ModelsModule/Models/libModels.a

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../../UtilitiesModule/Utilities/release/ -lUtilities
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../../UtilitiesModule/Utilities/debug/ -lUtilities
else:unix: LIBS += -L$$OUT_PWD/../../UtilitiesModule/Utilities/ -lUtilities

INCLUDEPATH += $$PWD/../../UtilitiesModule/Utilities
DEPENDPATH += $$PWD/../../UtilitiesModule/Utilities

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../UtilitiesModule/Utilities/release/libUtilities.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../UtilitiesModule/Utilities/debug/libUtilities.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../UtilitiesModule/Utilities/release/Utilities.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../UtilitiesModule/Utilities/debug/Utilities.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../../UtilitiesModule/Utilities/libUtilities.a
