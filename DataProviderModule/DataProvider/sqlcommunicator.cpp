/****************************************************************************
**
** Copyright (C) 2018  Jakub Wosiak
** Contact: jakubwosiak@gmail.com
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#include "sqlcommunicator.h"
#include <QString>
#include <QSqlDatabase>
#include <QtDebug>
#include <QSqlRecord>
#include <QJsonValue>
#include <QJsonObject>

QSqlDatabase *SqlCommunicator::getDatabase() const
{
    return database;
}

void SqlCommunicator::setDatabase(QSqlDatabase *value)
{
    database = value;
}

QSqlDatabase *SqlCommunicator::connectWithDB()
{
    ConfigReader *conf = ConfigReader::getInstance();
    QJsonObject *doc = conf->getConfigRoot();
    QJsonObject dbObj = conf->value(*doc, "database").toObject();

    QSqlDatabase *db = new QSqlDatabase(QSqlDatabase::addDatabase("QPSQL"));

    db->setConnectOptions();

    db->setHostName("localhost");
    db->setDatabaseName(conf->value(dbObj, "name").toString());
    db->setUserName(conf->value(dbObj, "user").toString());
    db->setPassword(conf->value(dbObj, "password").toString());

    db->setPort(5432);

    if (db->open()){
        qDebug() << "Opened database";
    } else {
        qDebug() << "Failed to open database";
    }

    return db;
}

SqlCommunicator::SqlCommunicator()
{

}


Record *SqlCommunicator::qSqlRecordToMap(QSqlRecord r)
{
    Record *result = new Record();

    for (int i = 0; i < r.count(); i++)
    {
        result->insert(r.fieldName(i), r.value(i));
    }

    return result;
}
