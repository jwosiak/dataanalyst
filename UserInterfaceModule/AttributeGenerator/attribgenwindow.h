/****************************************************************************
**
** Copyright (C) 2018  Jakub Wosiak
** Contact: jakubwosiak@gmail.com
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#ifndef ATTRIBGENWINDOW_H
#define ATTRIBGENWINDOW_H

#include <QMainWindow>
#include "generatorthread.h"

namespace Ui {
class AttribGenWindow;
}

class GenericDataProvider;

class AttribGenWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit AttribGenWindow(QWidget *parent = 0);
    ~AttribGenWindow();

    void updateProgressBar(int value);
private:
    Ui::AttribGenWindow *ui;
    GeneratorThread *genThread = nullptr;
private Q_SLOTS:
    void updateGenTarget(const QString &subjectName);
    void toggleBlockGenButton();
    void blockInputs();
    void unBlockInputs();
    void abortGeneration();
    void generateAttribute();
    void resetState();

    friend class GeneratorThread;
};

#endif // ATTRIBGENWINDOW_H
