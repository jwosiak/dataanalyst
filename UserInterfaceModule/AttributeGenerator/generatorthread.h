/****************************************************************************
**
** Copyright (C) 2018  Jakub Wosiak
** Contact: jakubwosiak@gmail.com
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#ifndef GENERATORTHREAD_H
#define GENERATORTHREAD_H

#include <QThread>
#include <genericdataprovider.h>

class AttribGenWindow;

class GeneratorThread : public QThread
{
    Q_OBJECT
public:
    GeneratorThread(AttribGenWindow *parentWindow);

    void setMustFinish(bool value);

protected:
    virtual void run();

private:
    AttribGenWindow *parentWindow;
    bool mustFinish = false;

    void computeRatingAttribute(QList<ValueWithIds> *records);

    void insertNewAxesToRelatedProfiles(QString attrSqlName,
                                        QString attrInProgramName);
};

#endif // GENERATORTHREAD_H
