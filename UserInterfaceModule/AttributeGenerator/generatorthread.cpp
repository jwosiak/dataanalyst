/****************************************************************************
**
** Copyright (C) 2018  Jakub Wosiak
** Contact: jakubwosiak@gmail.com
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#include "generatorthread.h"

#include <databaseupdator.h>
#include <continuousdatatype.h>
#include <numbertype.h>

#include "attribgenwindow.h"
#include "ui_attribgenwindow.h"

#include <algorithm>
#include <QDebug>
#include <QSqlDatabase>
#include <QtMath>

#include <QJsonObject>

static ConfigReader *conf = ConfigReader::getInstance();
static GenericDataProvider *dataProvider = GenericDataProvider::getInstance();
static DatabaseUpdator *updator = DatabaseUpdator::getInstance();

GeneratorThread::GeneratorThread(AttribGenWindow *parentWindow)
    : parentWindow(parentWindow)
{

}

void GeneratorThread::run()
{
    const int category = parentWindow->ui->genObject->currentIndex();
    const QString attrName = parentWindow->ui->attrName->toPlainText();
    const QString attrName2 = parentWindow->ui->attrName_2->toPlainText();
    const QString column = parentWindow->ui->genTarget->currentText();
    const QString destTable =
            conf->value(conf->attribGen(), "destination table").toString();

    QString cacheName = updator->
            addColumnAndTempTable(destTable,
                                  updator->realColumnParams(attrName));

    GroupedValues gvs = dataProvider->selectColumnForGen(category, column);

    if (!mustFinish)
    {
        for (QList<ValueWithIds> *val : gvs.values())
        {
            computeRatingAttribute(val);
        }
    }

    parentWindow->updateProgressBar(30);

    const int batch = gvs.count() / 5;
    for (int i = 0; i < gvs.count() && !mustFinish; i += batch)
    {
        updator->insertIntoTempTable(cacheName, attrName, gvs, i, batch);
        updator->updateColumn(destTable, attrName, cacheName);

        parentWindow->updateProgressBar(30 + (70 / 5) * (i / batch+1));
    }

    for (QList<ValueWithIds> *val : gvs.values())
    {
        for (ValueWithIds &id : *val)
        {
            delete id.first;
        }
    }

    if (!mustFinish)
    {
        parentWindow->updateProgressBar(100);
        insertNewAxesToRelatedProfiles(attrName, attrName2);
    }

    parentWindow->unBlockInputs();
}

void GeneratorThread::computeRatingAttribute(QList<ValueWithIds> *records)
{
    QList<qreal> values;
    const double c = parentWindow->ui->cValue->value();
    const double p = parentWindow->ui->pValue->value();
    double mi = 0., sigma = 0., N = records->length();

    for (const ValueWithIds &v : *records)
    {
        double x = v.first->toReal();
        mi += x;
        sigma += x*x;

        values.append(x);
    }

    mi /= N;
    sigma /= N;
    sigma -= mi*mi;

    sigma = std::sqrt(sigma);

    QList<ValueWithIds>::iterator iter = records->begin();
    for (qreal x : values)
    {
        double standarized = sigma == 0.? 0. : (x - mi) / sigma;
        qreal pow = qPow(standarized, p);
        qreal y =  pow * c;
        delete (*iter).first;
        (*iter).first = new NumberType(y);
        iter++;
    }
}

void GeneratorThread::insertNewAxesToRelatedProfiles(QString attrSqlName,
                                                     QString attrInProgramName)
{
    conf->insertNewAxis(attrSqlName, "number", attrInProgramName);
}

void GeneratorThread::setMustFinish(bool value)
{
    mustFinish = value;
}
