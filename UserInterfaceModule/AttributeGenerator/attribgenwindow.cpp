/****************************************************************************
**
** Copyright (C) 2018  Jakub Wosiak
** Contact: jakubwosiak@gmail.com
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#include "attribgenwindow.h"
#include "ui_attribgenwindow.h"
#include <genericdataprovider.h>
#include <configreader.h>
#include <genericfactory.h>

static ConfigReader *conf = ConfigReader::getInstance();
static GenericDataProvider *provider = GenericDataProvider::getInstance();
static GenericFactory *factory = GenericFactory::getInstance();

AttribGenWindow::AttribGenWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::AttribGenWindow)
{
    ui->setupUi(this);

    QObject::connect(ui->genObject, SIGNAL(currentIndexChanged(const QString)), this,
                     SLOT(updateGenTarget(const QString)));

    QObject::connect(ui->attrName, SIGNAL(textChanged()), this,
                     SLOT(toggleBlockGenButton()));
    QObject::connect(ui->attrName_2, SIGNAL(textChanged()), this,
                     SLOT(toggleBlockGenButton()));

    QObject::connect(ui->genButton, SIGNAL(pressed()), this,
                     SLOT(generateAttribute()));

    QObject::connect(ui->genButton, SIGNAL(pressed()), this,
                     SLOT(blockInputs()));

    QObject::connect(ui->abortButton, SIGNAL(pressed()), this,
                     SLOT(abortGeneration()));

    QObject::connect(ui->genObject, SIGNAL(currentIndexChanged(int)), this,
                     SLOT(resetState()));
    QObject::connect(ui->genTarget, SIGNAL(currentIndexChanged(int)), this,
                     SLOT(resetState()));

    ui->genObject->addItems(conf->selectCategories());
    toggleBlockGenButton();
}

AttribGenWindow::~AttribGenWindow()
{
    delete ui;
}

void AttribGenWindow::updateGenTarget(const QString &subjectName)
{
    if (subjectName.isEmpty())
    {
        return;
    }

    ui->genTarget->blockSignals(true);
    ui->genTarget->clear();

    for (const QString &type : conf->selectAxesTypes())
    {
        if (factory->isContinuous(conf->axisType(type)))
        {
            ui->genTarget->addItem(type);
        }
    }
    ui->genTarget->blockSignals(false);
}

void AttribGenWindow::toggleBlockGenButton()
{
    bool oneIsEmpty = ui->attrName->toPlainText().isEmpty() ||
            ui->attrName_2->toPlainText().isEmpty();
    ui->genButton->setEnabled(!oneIsEmpty);
}

void AttribGenWindow::blockInputs()
{
    ui->genButton->setEnabled(false);
    ui->genTarget->setEnabled(false);
    ui->genTarget->setEnabled(false);
    ui->genObject->setEnabled(false);
    ui->attrName->setEnabled(false);
    ui->attrName_2->setEnabled(false);
    ui->pValue->setEnabled(false);
    ui->cValue->setEnabled(false);
}

void AttribGenWindow::unBlockInputs()
{
    ui->genButton->setEnabled(true);
    ui->genTarget->setEnabled(true);
    ui->genTarget->setEnabled(true);
    ui->genObject->setEnabled(true);
    ui->attrName->setEnabled(true);
    ui->attrName_2->setEnabled(true);
    ui->pValue->setEnabled(true);
    ui->cValue->setEnabled(true);
}

void AttribGenWindow::updateProgressBar(int value)
{
    ui->genProgress->setValue(value);
}

void AttribGenWindow::abortGeneration()
{
    if (genThread != nullptr)
    {
        genThread->setMustFinish(true);
        genThread->wait();
        delete genThread;
        genThread = nullptr;
        unBlockInputs();
        resetState();
    }
}

void AttribGenWindow::generateAttribute()
{
    genThread = new GeneratorThread(this);
    genThread->start();
}

void AttribGenWindow::resetState()
{
    updateProgressBar(0);
}
