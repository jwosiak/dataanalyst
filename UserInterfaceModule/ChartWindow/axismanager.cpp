/****************************************************************************
**
** Copyright (C) 2018  Jakub Wosiak
** Contact: jakubwosiak@gmail.com
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#include "axismanager.h"
#include <QCategoryAxis>

QCategoryAxis *AxisManager::getAxis() const
{
    return axis;
}

qreal AxisManager::getMarginFactor() const
{
    return marginFactor;
}

void AxisManager::setMarginFactor(const qreal &value)
{
    marginFactor = value;
}

qreal AxisManager::getZoom() const
{
    return zoom;
}

void AxisManager::setZoom(const qreal &value)
{
    zoom = value;
}

qreal AxisManager::getOffset() const
{
    return offset;
}

void AxisManager::setOffset(const qreal &value)
{
    offset = value;
}

qreal AxisManager::getMin() const
{
    return min;
}

void AxisManager::setMin(const qreal &value)
{
    min = value;
}

qreal AxisManager::getMax() const
{
    return max;
}

void AxisManager::setMax(const qreal &value)
{
    max = value;
}

AxisManager::AxisManager(qreal min, qreal max, qreal marginFactor)
    : AxisManager(marginFactor)
{
    this->min = min;
    this->max = max;
}

AxisManager::AxisManager(qreal marginFactor)
    : marginFactor(marginFactor)
{
    axis = new QCategoryAxis();
}

AxisManager::~AxisManager()
{
    delete axis;
}
