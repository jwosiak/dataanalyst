/****************************************************************************
**
** Copyright (C) 2018  Jakub Wosiak
** Contact: jakubwosiak@gmail.com
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#ifndef CHART2DMANAGER_H
#define CHART2DMANAGER_H

#include <QWidget>
#include <QtCharts/QChartGlobal>
#include "chartview.h"
#include "genericdataprovider.h"
#include "genericfactory.h"
#include <discretetypepointer.h>
#include <QSet>

class AxisManager;

namespace Ui{
class ChartWindow;
}

QT_CHARTS_BEGIN_NAMESPACE
class QXYSeries;
class QBoxSet;
class QBarSet;
QT_CHARTS_END_NAMESPACE

QT_CHARTS_USE_NAMESPACE

typedef QMap<DiscreteTypePointer, QSharedPointer<QList<qreal> > > CategorizedSeries;
typedef QMap<DiscreteTypePointer, QSharedPointer<CategorizedSeries> > MultiCategorizedSeries;
typedef QPair<MultiCategorizedSeries, QSet<DiscreteTypePointer> > CategorizationResult;

class Chart2DManager : public QObject
{
    Q_OBJECT
public:
    explicit Chart2DManager(Ui::ChartWindow *ui, QObject *parent = 0);
    ~Chart2DManager();

    void drawBlankChart();

    void drawCartesianChart(QList<DataRecord *> &records, Record *groupedBy,
                            QString xAxisLabel, QString yAxisLabel,
                            QString xAxisType, QString yAxisType);
    void drawBoxChart(QList<DataRecord *> &records, Record *groupedBy,
                      Record *groupedBy2, QString yAxisLabel,
                      QString yAxisType);
    void drawBarChart(QList<DataRecord *> &records, Record *groupedBy,
                      Record *groupedBy2, QString yAxisLabel);
    QWidget *getChartWidget() const;

    void setXAxis(AxisManager *value);

    void setYAxis(AxisManager *value);

private:
    Ui::ChartWindow *ui;
    ChartView *chartView;
    QWidget *chartWidget;

    QString groupingLabel;
    QString groupingLabel2;
    qreal minX, maxX, minY, maxY;


    QMap<QPair<qreal, qreal>, DataRecord *> pointToRecord;
    QMap<QBoxSet *, QMap<QString, QString> > boxToRecord;
    QMap<QBarSet *, QList<QMap<QString, QString> > > barSetToRecords;
    QChart *lastChart = nullptr;

    AxisManager *xAxis;
    AxisManager *yAxis;

    void preprocessData(QList<DataRecord *> &records, Record *groupedBy,
                        QString sortedBy);

    CategorizationResult divideDataIntoCategories(QList<DataRecord *> &records,
                                               QString yAxisLabel);

    void removeUnboxableCategories(MultiCategorizedSeries &cs,
                                   QSet<DiscreteTypePointer> &innerCats);

    ContinuousDataType *cont(DataType *value) const;
    qreal findMedian(QList<qreal> *values, int begin, int end) const;

    void scaleAxes();

    void zoomAxis(AxisManager *axis, int percent);

    void clearLastChart();

    QPointF pairToPoint(const QPair<qreal, qreal> &p);
    qreal distance(const QPointF &p1, const QPointF &p2);
private Q_SLOTS:
    void updateChartInfo(const QPointF &point);
    void updateChartInfo(QBoxSet *boxSet);
    void updateChartInfo(int index, QBarSet *barSet);
    void zoomX(int percent);
    void zoomY(int percent);
};

#endif // CHART2DMANAGER_H
