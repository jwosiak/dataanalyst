/****************************************************************************
**
** Copyright (C) 2018  Jakub Wosiak
** Contact: jakubwosiak@gmail.com
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#ifndef CONTINUOUSAXISMANAGER_H
#define CONTINUOUSAXISMANAGER_H

#include "axismanager.h"
#include <functional>

typedef std::function<QString(qreal)> LabelFormatter;

class ContinuousAxisManager : public AxisManager
{
private:
    int ticks;
    LabelFormatter format;
public:
    ContinuousAxisManager(qreal min, qreal max, int ticks,
                          LabelFormatter format, qreal marginFactor = 0.05);

public:
    virtual void update();
    int getTicks() const;
    void setTicks(int value);

private:
    void clearAxisLabels();
};

#endif // CONTINUOUSAXISMANAGER_H
