/****************************************************************************
**
** Copyright (C) 2018  Jakub Wosiak
** Contact: jakubwosiak@gmail.com
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#ifndef AXISMANAGER_H
#define AXISMANAGER_H

#include <QtCharts/QChartGlobal>

QT_CHARTS_BEGIN_NAMESPACE
class QCategoryAxis;
QT_CHARTS_END_NAMESPACE

QT_CHARTS_USE_NAMESPACE

class AxisManager
{
protected:
    QCategoryAxis *axis;
    qreal min;
    qreal max;
    qreal marginFactor;
    qreal offset = 0;
    qreal zoom = 1.0;
public:
    AxisManager(qreal min, qreal max, qreal marginFactor = 0.05);
    AxisManager(qreal marginFactor = 0.05);
    virtual ~AxisManager();

    QCategoryAxis *getAxis() const;
    qreal getMarginFactor() const;
    void setMarginFactor(const qreal &value);
    qreal getZoom() const;
    void setZoom(const qreal &value);

    virtual void update() = 0;
    qreal getOffset() const;
    void setOffset(const qreal &value);
    qreal getMin() const;
    void setMin(const qreal &value);
    qreal getMax() const;
    void setMax(const qreal &value);
};

#endif // AXISMANAGER_H
