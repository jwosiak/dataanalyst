/****************************************************************************
**
** Copyright (C) 2018  Jakub Wosiak
** Contact: jakubwosiak@gmail.com
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#include "chartview.h"
#include "ui_chartwindow.h"

AxisManager *ChartView::getXAxis() const
{
    return xAxis;
}

void ChartView::setXAxis(AxisManager *value)
{
    xAxis = value;
}

AxisManager *ChartView::getYAxis() const
{
    return yAxis;
}

void ChartView::setYAxis(AxisManager *value)
{
    yAxis = value;
}

Ui::ChartWindow *ChartView::getUi() const
{
    return ui;
}

void ChartView::setUi(Ui::ChartWindow *value)
{
    ui = value;
}

ChartView::ChartView(QWidget *parent)
    : QChartView(parent)
{

}

ChartView::ChartView(QChart* chart, QWidget *parent)
    : QChartView(chart, parent)
{

}

void ChartView::mousePressEvent(QMouseEvent *event)
{
    QChartView::mousePressEvent(event);
    lastMousePos = event->localPos();
    mousePressed = true;
}

void ChartView::mouseReleaseEvent(QMouseEvent *event)
{
    QChartView::mouseReleaseEvent(event);
    mousePressed = false;
}

void ChartView::mouseMoveEvent(QMouseEvent *event)
{
    QChartView::mouseMoveEvent(event);
    if (!mousePressed || xAxis == nullptr || yAxis == nullptr)
    {
        return;
    }

    qreal dx = (lastMousePos.x() - event->localPos().x()) / this->width();
    qreal dy = (lastMousePos.y() - event->localPos().y()) / this->height();

    qreal width = xAxis->getMax() - xAxis->getMin();
    xAxis->setOffset(xAxis->getOffset() - (width * dx) / xAxis->getZoom());

    qreal height = yAxis->getMax() - yAxis->getMin();
    yAxis->setOffset(yAxis->getOffset() + height * dy / yAxis->getZoom());

    xAxis->update();
    yAxis->update();
    lastMousePos = event->localPos();
}

void ChartView::wheelEvent(QWheelEvent *event)
{
    bool direction = event->delta() > 0;
    zoomSingleAxis(direction, xAxis, ui->xZoom);
    zoomSingleAxis(direction, yAxis, ui->yZoom);
}

void ChartView::zoomSingleAxis(bool direction, AxisManager *axis, QSpinBox *zoomBox)
{
    if (axis != nullptr && zoomBox->isEnabled())
    {
        int steps = zoomBox->value() / 10;
        int d = (zoomBox->singleStep() * steps) * (direction? 1 : -1);
        int v = zoomBox->value() + d;
        v = v > 100? v : 100;
        zoomBox->setValue(v);
        updateZoom(axis, v);
    }
}

void ChartView::updateZoom(AxisManager *axis, int percent)
{
    if (axis != nullptr)
    {
        axis->setZoom(qreal(percent) / 100.);
        axis->update();
    }
}
