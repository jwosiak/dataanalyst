#-------------------------------------------------
#
# Project created by QtCreator 2017-10-27T17:54:14
#
#-------------------------------------------------

QT       += core gui charts sql network widgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = $$OUT_PWD/../../DataAnalyst
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
    axismanager.cpp \
    chart2dmanager.cpp \
    chartwindow.cpp \
    continuousaxismanager.cpp \
    discreteaxismanager.cpp \
    chartview.cpp \
    main.cpp \
    mainwindowmanager.cpp

HEADERS += \
    axismanager.h \
    chart2dmanager.h \
    chartwindow.h \
    continuousaxismanager.h \
    discreteaxismanager.h \
    chartview.h \
    mainwindowmanager.h

FORMS += \
        chartwindow.ui

SUBDIRS += \
    ChartWidgets.pro


win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../AttributeGenerator/release/ -lAttributeGenerator
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../AttributeGenerator/debug/ -lAttributeGenerator
else:unix: LIBS += -L$$OUT_PWD/../AttributeGenerator/ -lAttributeGenerator

INCLUDEPATH += $$PWD/../AttributeGenerator
DEPENDPATH += $$PWD/../AttributeGenerator

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../AttributeGenerator/release/libAttributeGenerator.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../AttributeGenerator/debug/libAttributeGenerator.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../AttributeGenerator/release/AttributeGenerator.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../AttributeGenerator/debug/AttributeGenerator.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../AttributeGenerator/libAttributeGenerator.a


win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../../DataProviderModule/DataProvider/release/ -lDataProvider
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../../DataProviderModule/DataProvider/debug/ -lDataProvider
else:unix: LIBS += -L$$OUT_PWD/../../DataProviderModule/DataProvider/ -lDataProvider

INCLUDEPATH += $$PWD/../../DataProviderModule/DataProvider
DEPENDPATH += $$PWD/../../DataProviderModule/DataProvider

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../DataProviderModule/DataProvider/release/libDataProvider.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../DataProviderModule/DataProvider/debug/libDataProvider.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../DataProviderModule/DataProvider/release/DataProvider.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../DataProviderModule/DataProvider/debug/DataProvider.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../../DataProviderModule/DataProvider/libDataProvider.a

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../../ModelsModule/Models/release/ -lModels
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../../ModelsModule/Models/debug/ -lModels
else:unix: LIBS += -L$$OUT_PWD/../../ModelsModule/Models/ -lModels

INCLUDEPATH += $$PWD/../../ModelsModule/Models
DEPENDPATH += $$PWD/../../ModelsModule/Models

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../ModelsModule/Models/release/libModels.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../ModelsModule/Models/debug/libModels.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../ModelsModule/Models/release/Models.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../ModelsModule/Models/debug/Models.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../../ModelsModule/Models/libModels.a

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../../UtilitiesModule/Utilities/release/ -lUtilities
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../../UtilitiesModule/Utilities/debug/ -lUtilities
else:unix: LIBS += -L$$OUT_PWD/../../UtilitiesModule/Utilities/ -lUtilities

INCLUDEPATH += $$PWD/../../UtilitiesModule/Utilities
DEPENDPATH += $$PWD/../../UtilitiesModule/Utilities
