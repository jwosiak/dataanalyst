/****************************************************************************
**
** Copyright (C) 2018  Jakub Wosiak
** Contact: jakubwosiak@gmail.com
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#ifndef MAINWINDOWMANAGER_H
#define MAINWINDOWMANAGER_H

#include <QObject>
#include <QListWidget>
#include <QPushButton>
#include <QTextEdit>

#include <genericdataprovider.h>

namespace Ui {
class ChartWindow;
}
class Chart2DManager;
class QComboBox;
class QSpinBox;

typedef QPair<QString, QList<DataRecord *>> NamedRecords;

class MainWindowManager : public QObject
{
    Q_OBJECT
public:
    explicit MainWindowManager(Ui::ChartWindow *ui,
                                Chart2DManager *chartManager,
                                QObject *parent = 0);
    ~MainWindowManager();

    QWidget *getRoot() const;

private:
    int chartCategory = 0;

    Chart2DManager *chartManager;

    QList<Record *> selectedFilters;

    const int PAGE_SIZE;
    int maxPageNumber;

    QList<QVariant> elements;

    QWidget *root;
    Ui::ChartWindow *ui;

    QList<QString> filtersToString();
    QString interpolateWithParams(const QString &s);
    void clearParams();

    QPair<Record *, Record *> groupedBy();

    NamedRecords currentRecords;
    bool areSaved(QString recordsName);
    void deleteRecords(QList<DataRecord *> &records);
    void updateCurrentRecords();
    QList<NamedRecords> savedRecords;
    QList<DataRecord *> allRecords();

    Record *attachIntervals(QList<DataRecord *> &records, Record *grouper,
                            int id, int intervCount);

    void updateGroupingSelect(QComboBox *groupingSelect);
    void toggleIntervalNum(QComboBox *groupingSelect, QSpinBox *intervalNum);
    bool recentlyAttachedIntervals(QList<DataRecord *> &records, int groupId);
    void clearUnusedAttachedIntervals(QList<DataRecord *> &records,
                                      int groupId);
private Q_SLOTS:
    void addFilter();
    void removeFilter();
    void updateChartCategory(int category);

    void resetInterface();
    void updateMaxPage();
    void updateChartType();
    void updateSelectedFilters();
    void updateFiltersList();
    void updateElementsListSorter();
    void updateElementsList();
    void deselectCurrentElement(QListWidgetItem* item);
    void updateGroupingSelects();
    void updateChartSubject();
    void updateAxesSelectors();
    void updateSavedList();
    void saveNextRecords();
    void clearSavedRecords();
    void updateIntervalsNum();
    void openAttribGenWindow();

    void reloadConfig();
    void reopenDatabase();
};

#endif // MAINWINDOWMANAGER_H
