/****************************************************************************
**
** Copyright (C) 2018  Jakub Wosiak
** Contact: jakubwosiak@gmail.com
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#include <QApplication>

#include <QtGlobal>
#include <QJsonObject>
#include <QSqlDatabase>

#include "chart2dmanager.h"
#include "mainwindowmanager.h"
#include "chartwindow.h"

#include <configreader.h>
#include <continuousintervaltype.h>
#include <genericdataprovider.h>
#include <numericstringtype.h>
#include <stringtype.h>
#include <timertype.h>
#include <genericfactory.h>
#include <datetype.h>
#include <numbertype.h>
#include <sqlcommunicator.h>
#include <databaseupdator.h>


int main(int argc, char *argv[])
{

    // init ConfigReader and establish database connection

    QString configPath = argc > 1? argv[1] : "config.json";


    ConfigReader *conf = ConfigReader::getInstance();
    conf->loadConfigFile(configPath);

    QSqlDatabase *sqlDatabase = SqlCommunicator::connectWithDB();

    GenericDataProvider::getInstance()->setDatabase(sqlDatabase);
    DatabaseUpdator::getInstance()->setDatabase(sqlDatabase);



    // init built-in types environment

    GenericFactory *typeMappings = GenericFactory::getInstance();
    typeMappings->addItem("time interval",
                          [](QVariant v) { return new TimerType(v.toTime()); },
                          TimerType::format);
    typeMappings->addItem("date",
                          [](QVariant v) { return new DateType(v.toDateTime()); },
                          DateType::format);

    typeMappings->addItem("number",
                          [](QVariant v) { return new NumberType(v.toReal()); },
                          NumberType::format);

    typeMappings->addItem("int",
                         [](QVariant v) { return new NumberType(v.toReal()); },
                         NumberType::intFormat);

    typeMappings->addItem("string",
                          [](QVariant v) { return new StringType(v.toString()); });

    typeMappings->addItem("numeric string",
                          [](QVariant v) { return new NumericStringType(v.toString()); });

    typeMappings->registerContinuousType("time interval");
    typeMappings->registerContinuousType("date");
    typeMappings->registerContinuousType("number");
    typeMappings->registerContinuousType("int");

    typeMappings->registerDiscreteType("string");
    typeMappings->registerDiscreteType("numeric string");
    typeMappings->registerDiscreteType("continuous interval");



    // init application and window

    QApplication a(argc, argv);
    ChartWindow w;
    w.show();

    Chart2DManager *chartManager = new Chart2DManager(w.getUi());
    MainWindowManager *filterManager =
            new MainWindowManager(w.getUi(), chartManager);

    // perform any action to get rid of compiler warning
    filterManager->getRoot();

    return a.exec();
}
