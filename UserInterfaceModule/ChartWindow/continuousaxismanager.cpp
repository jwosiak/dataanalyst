/****************************************************************************
**
** Copyright (C) 2018  Jakub Wosiak
** Contact: jakubwosiak@gmail.com
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#include "continuousaxismanager.h"
#include <QCategoryAxis>
#include <algorithm>


int ContinuousAxisManager::getTicks() const
{
    return ticks;
}

void ContinuousAxisManager::setTicks(int value)
{
    ticks = value;
}

void ContinuousAxisManager::clearAxisLabels()
{
    for (QString label : axis->categoriesLabels())
    {
        axis->remove(label);
    }
}

ContinuousAxisManager::ContinuousAxisManager(qreal min, qreal max, int ticks,
                                             LabelFormatter format,
                                             qreal marginFactor)
    : AxisManager (min, max, marginFactor)
    , ticks(ticks)
    , format(format)
{
   update();
}

void ContinuousAxisManager::update()
{   
    qreal dist = max - min;
    qreal leftEdge, rightEdge;
    clearAxisLabels();

    if (dist < 0.0001 )
    {
        axis->append(format(min), min);
        leftEdge = min - 1;
        rightEdge = min + 1;
        axis->setTickCount(2);
    }
    else
    {
        qreal margin = dist * marginFactor;
        qreal zoomOffset = (dist - dist / zoom) / 2.;

        offset = offset > zoomOffset? zoomOffset : offset;
        qreal width = dist + 2*margin - 2*zoomOffset;

        if (min - margin + zoomOffset + width - offset > max + margin)
        {
            offset = min + zoomOffset + width - max - 2*margin;
        }

        leftEdge = min - margin + zoomOffset - offset;
        rightEdge = leftEdge + width;

        for (int i = 0; i <= ticks; i++)
        {
            qreal value = min + static_cast<qreal>(i) * (dist / ticks);
            if (value >= leftEdge && value <= rightEdge)
            {
                axis->append(format(value), value);
            }
        }
        axis->setTickCount(ticks);
    }
    axis->setLabelsPosition(QCategoryAxis::AxisLabelsPositionOnValue);
    axis->setMin(leftEdge);
    axis->setMax(rightEdge);
}
