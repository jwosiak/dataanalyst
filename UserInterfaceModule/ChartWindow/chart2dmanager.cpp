/****************************************************************************
**
** Copyright (C) 2018  Jakub Wosiak
** Contact: jakubwosiak@gmail.com
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#include "chart2dmanager.h"
#include "timertype.h"
#include "datetype.h"
#include "continuousaxismanager.h"
#include "discreteaxismanager.h"
#include "ui_chartwindow.h"

#include <QScatterSeries>
#include <QLineSeries>
#include <QtCharts/QBoxSet>
#include <QList>
#include <QPointF>
#include <QValueAxis>
#include <QDateTimeAxis>
#include <QCategoryAxis>
#include <algorithm>
#include <limits>
#include <iostream>
#include <QGridLayout>
#include <QColor>
#include <QLegendMarker>
#include <QBoxPlotSeries>
#include <QBarSet>
#include <QBarSeries>
#include <QSharedPointer>
#include <discretedatatype.h>
#include <numericstringtype.h>
#include <QtMath>
#include <QSet>


static GenericFactory *fact = GenericFactory::getInstance();

Chart2DManager::Chart2DManager(Ui::ChartWindow *ui, QObject *parent)
    : QObject (parent)
    , ui(ui)
{
    chartView = ui->chartWidget;
    chartView->setUi(ui);

    QObject::connect(ui->xZoom, SIGNAL(valueChanged(int)), this,
                     SLOT(zoomX(int)));

    QObject::connect(ui->yZoom, SIGNAL(valueChanged(int)), this,
                     SLOT(zoomY(int)));
}

Chart2DManager::~Chart2DManager()
{

}

void Chart2DManager::drawBlankChart()
{
    QChart *chart = new QChart();
    chartView->setChart(chart);

    clearLastChart();
    lastChart = chart;

    setXAxis(nullptr);
    setYAxis(nullptr);
}

void Chart2DManager::drawCartesianChart(QList<DataRecord *> &records,
                                        Record *groupedBy, QString xAxisLabel,
                                        QString yAxisLabel, QString xAxisType,
                                        QString yAxisType)
{
    const bool hasGroupper = !groupedBy->empty();
    QChart *chart = new QChart();
    QMap<DiscreteTypePointer, QList<QXYSeries *>> categorizedSeries;
    minX = minY = std::numeric_limits<qreal>::max();
    maxX = maxY = std::numeric_limits<qreal>::min();
    pointToRecord.clear();

    preprocessData(records, groupedBy, xAxisLabel);

    QList<QPointF> points;

    for (DataRecord *r : records)
    {
        qreal x = cont(r->value(xAxisLabel))->toReal();
        qreal y = cont(r->value(yAxisLabel))->toReal();
        QPointF point(x, y);

        pointToRecord.insert(QPair<qreal, qreal>(x, y), r);
        points.append(point);
        DiscreteDataType *category = hasGroupper
                ? static_cast<DiscreteDataType *>(r->value(groupingLabel))
                : new StringType("");

        DiscreteTypePointer pCategory(category);

        if (!categorizedSeries.contains(pCategory)){
            QScatterSeries *scatterSeries = new QScatterSeries();
            QLineSeries *lineSeries = new QLineSeries();
            QList<QXYSeries *> packedSeries;
            packedSeries.append(scatterSeries);
            packedSeries.append(lineSeries);
            categorizedSeries.insert(pCategory, packedSeries);

            scatterSeries->setName(category->toString());
            lineSeries->setName(category->toString());

//            scatterSeries->setUseOpenGL(true);

            scatterSeries->setMarkerShape(
                        QtCharts::QScatterSeries::MarkerShapeRectangle);

            QObject::connect(scatterSeries, SIGNAL(doubleClicked(QPointF)),
                             this, SLOT(updateChartInfo(QPointF)));

            scatterSeries->setMarkerSize(scatterSeries->markerSize() * 1.);
        }

        for (QXYSeries *series : categorizedSeries.value(category))
        {
            series->append(point);
        }

        minX = minX < x? minX : x;
        maxX = maxX > x? maxX : x;
        minY = minY < y? minY : y;
        maxY = maxY > y? maxY : y;
    }


    setXAxis(new ContinuousAxisManager(minX, maxX, 7,
                                       fact->getFormat(xAxisType), 0.03));
    setYAxis(new ContinuousAxisManager(minY, maxY, 10,
                                       fact->getFormat(yAxisType), 0.09));

    chart->addAxis(xAxis->getAxis(), Qt::AlignBottom);
    chart->addAxis(yAxis->getAxis(), Qt::AlignLeft);

    QList<DiscreteTypePointer> keys = categorizedSeries.keys();
    std::sort(keys.begin(), keys.end());

    for (const DiscreteTypePointer &key : keys)
    {
        QList<QXYSeries *> allSeries = categorizedSeries.value(key);
        chart->addSeries(allSeries.first());
        chart->addSeries(allSeries.last());
        allSeries.first()->setColor(allSeries.last()->color());

        for (QXYSeries *series : allSeries)
        {
            series->attachAxis(xAxis->getAxis());
            series->attachAxis(yAxis->getAxis());
        }
    }

    int i = 0;
    for (QLegendMarker *m : chart->legend()->markers())
    {
        m->setVisible( !(i++ % 2 || !hasGroupper));
    }

    chartView->setChart(chart);
    scaleAxes();

    clearLastChart();
    lastChart = chart;
}

void Chart2DManager::drawBoxChart(QList<DataRecord *> &records,
                                  Record *groupedBy, Record *groupedBy2,
                                  QString yAxisLabel, QString yAxisType)
{
    LabelFormatter format = fact->getFormat(yAxisType);
    QChart *chart = new QChart();
    QList<DiscreteDataType *> xAxisLabels;
    minY = std::numeric_limits<qreal>::max();
    maxY = std::numeric_limits<qreal>::min();

    boxToRecord.clear();
    preprocessData(records, groupedBy, yAxisLabel);
    groupingLabel2 = groupedBy2->value("label").toString();

    CategorizationResult cr = divideDataIntoCategories(records, yAxisLabel);

    removeUnboxableCategories(cr.first, cr.second);

    MultiCategorizedSeries categorizedSeries = cr.first;
    QList<DiscreteTypePointer> innerCategories = cr.second.toList();

    std::sort(innerCategories.begin(), innerCategories.end());

    MultiCategorizedSeries::key_value_iterator outerIter =
            categorizedSeries.keyValueBegin();

    for (; outerIter != categorizedSeries.keyValueEnd(); outerIter++)
    {
        QBoxPlotSeries *series = new QBoxPlotSeries(chart);
        DiscreteDataType *outerCategory = (*outerIter).first.getValue();
        const QString outerCategoryValue = outerCategory->toString();
        series->setName(outerCategoryValue);

        CategorizedSeries *innerMap = (*outerIter).second.data();
        for (const DiscreteTypePointer &p : innerCategories)
        {
            const QString innerCategoryValue = p.getValue()->toString();
            QBoxSet *boxSet = new QBoxSet(innerCategoryValue);
            CategorizedSeries::iterator innerIter = innerMap->find(p);
            int count = 0;
            qreal lowerExtreme, upperExtreme, median, lowerQuartile, upperQuartile;
            lowerExtreme = upperExtreme = median = lowerQuartile
                    = upperQuartile = -1000000000000.0;
            if (innerIter != innerMap->end())
            {
                QList<qreal> *data = innerIter.value().data();
                count = data->count();
                if (count >= 4)
                {
                    lowerExtreme = data->first();
                    upperExtreme = data->last();
                    median = findMedian(data, 0, count);
                    lowerQuartile = findMedian(data, 0, count / 2);
                    upperQuartile = findMedian(data, count / 2 + (count % 2), count);

                    QMap<QString, QString> r;
                    r.insert("Liczba elementów", QString::number(count));
                    r.insert("Min", format(lowerExtreme));
                    r.insert("Max", format(upperExtreme));
                    r.insert("Mediana", format(median));
                    r.insert("Dolny kwartyl", format(lowerQuartile));
                    r.insert("Górny kwartyl", format(upperQuartile));
                    if (!outerCategoryValue.isEmpty())
                    {
                        r.insert(groupingLabel,
                                 outerCategoryValue);
                    }
                    if (!innerCategoryValue.isEmpty())
                    {
                        r.insert(groupingLabel2,
                                 innerCategoryValue);
                    }
                    boxToRecord.insert(boxSet, r);

                    minY = minY < lowerExtreme? minY : lowerExtreme;
                    maxY = maxY > upperExtreme? maxY : upperExtreme;
                }
            }
            boxSet->setValue(QBoxSet::LowerExtreme, lowerExtreme);
            boxSet->setValue(QBoxSet::UpperExtreme, upperExtreme);
            boxSet->setValue(QBoxSet::Median, median);
            boxSet->setValue(QBoxSet::LowerQuartile, lowerQuartile);
            boxSet->setValue(QBoxSet::UpperQuartile, upperQuartile);
            series->append(boxSet);
        }
        chart->addSeries(series);
        QObject::connect(series, SIGNAL(doubleClicked(QBoxSet*)), this,
                         SLOT(updateChartInfo(QBoxSet*)));
    }

    for (DiscreteTypePointer &p : innerCategories)
    {
        xAxisLabels.append(p.getValue());
    }

    if (xAxisLabels.isEmpty())
    {
        xAxisLabels.append(new StringType(""));
    }

    chart->legend()->setVisible(!groupedBy->empty());

    setXAxis(new DiscreteAxisManager(xAxisLabels));
    setYAxis(new ContinuousAxisManager(minY, maxY, 10,
                                       fact->getFormat(yAxisType), 0));


    chart->addAxis(yAxis->getAxis(), Qt::AlignLeft);
    chart->addAxis(xAxis->getAxis(), Qt::AlignBottom);

    for (QAbstractSeries * series : chart->series())
    {
        series->attachAxis(yAxis->getAxis());
    }

    chartView->setChart(chart);
    scaleAxes();

    clearLastChart();
    lastChart = chart;
}

void Chart2DManager::drawBarChart(QList<DataRecord *> &records,
                                  Record *groupedBy, Record *groupedBy2,
                                  QString yAxisLabel)
{
    QChart *chart = new QChart();

    QList<DiscreteDataType *> xAxisLabels;
    barSetToRecords.clear();

    preprocessData(records, groupedBy, yAxisLabel);
    groupingLabel2 = groupedBy2->value("label").toString();

    CategorizationResult cr = divideDataIntoCategories(records, yAxisLabel);

    MultiCategorizedSeries categorizedSeries = cr.first;
    QList<DiscreteTypePointer> innerCategories = cr.second.toList();

    std::sort(innerCategories.begin(), innerCategories.end());

    QList<DiscreteTypePointer> outerKeys = categorizedSeries.keys();
    std::sort(outerKeys.begin(), outerKeys.end());

    qreal maxHeight = 0;


    MultiCategorizedSeries::key_value_iterator outerIter =
            categorizedSeries.keyValueBegin();

    QBarSeries *series = new QBarSeries(chart);
    for (; outerIter != categorizedSeries.keyValueEnd(); outerIter++)
    {
        DiscreteDataType *outerCategory = (*outerIter).first.getValue();
        const QString outerCategoryValue = outerCategory->toString();
        QBarSet *barSet = new QBarSet(outerCategory->toString());

        QList<QMap<QString, QString> > setRecords;

        CategorizedSeries *innerMap = (*outerIter).second.data();
        for (const DiscreteTypePointer &p : innerCategories)
        {
            CategorizedSeries::iterator innerIter = innerMap->find(p);
            const QString innerCategoryValue = p.getValue()->toString();
            int count = 0;
            if (innerIter != innerMap->end())
            {
                count = innerIter.value().data()->count();
                maxHeight = maxHeight > count?
                            maxHeight : count;
            }
            barSet->append(count);

            QMap<QString, QString> r;
            if (!outerCategoryValue.isEmpty())
            {
                r.insert(groupingLabel,
                         outerCategoryValue);
            }
            if (!innerCategoryValue.isEmpty())
            {
                r.insert(groupingLabel2,
                         innerCategoryValue);
            }
            setRecords.append(r);
        }
        series->append(barSet);
        barSetToRecords.insert(barSet, setRecords);
    }

    QObject::connect(series, SIGNAL(doubleClicked(int, QBarSet*)), this,
                     SLOT(updateChartInfo(int, QBarSet*)));
    chart->addSeries(series);

    for (DiscreteTypePointer &p : innerCategories)
    {
        xAxisLabels.append(p.getValue());
    }

    if (xAxisLabels.isEmpty())
    {
        xAxisLabels.append(new StringType(""));
    }

    setXAxis(new DiscreteAxisManager(xAxisLabels));
    setYAxis(new ContinuousAxisManager(0, maxHeight,
                                       categorizedSeries.keys().count(),
                                       fact->getFormat("number"), 0));

    chart->addAxis(yAxis->getAxis(), Qt::AlignLeft);
    chart->addAxis(xAxis->getAxis(), Qt::AlignBottom);

    chart->legend()->setVisible(!groupedBy->empty());

    for (QAbstractSeries * series : chart->series())
    {
        series->attachAxis(yAxis->getAxis());
    }

    chartView->setChart(chart);
    scaleAxes();

    clearLastChart();
    lastChart = chart;
}

QWidget *Chart2DManager::getChartWidget() const
{
    return chartWidget;
}

void Chart2DManager::setXAxis(AxisManager *value)
{
    xAxis = value;
    chartView->setXAxis(value);
}

void Chart2DManager::setYAxis(AxisManager *value)
{
    yAxis = value;
    chartView->setYAxis(value);
}

void Chart2DManager::preprocessData(QList<DataRecord *> &records,
                                    Record *groupedBy, QString sortedBy)
{
    groupingLabel = groupedBy->value("label").toString();

    std::sort(records.begin(), records.end(),
          [&sortedBy, this](DataRecord *r1, DataRecord *r2)
    { return *cont(r1->value(sortedBy)) < *cont(r2->value(sortedBy)); });
}

CategorizationResult Chart2DManager::divideDataIntoCategories(QList<DataRecord *> &records,
                                                              QString yAxisLabel)
{
    MultiCategorizedSeries categorizedSeries;
    QSet<DiscreteTypePointer> innerCategories;

    for (DataRecord *r : records)
    {
        qreal y = cont(r->value(yAxisLabel))->toReal();

        DiscreteDataType *category1Value =
                groupingLabel.isEmpty()
                ? new StringType("")
                : dynamic_cast<DiscreteDataType *>(r->value(groupingLabel));

        DiscreteDataType *category2Value =
                groupingLabel2.isEmpty()
                ? new StringType("")
                : dynamic_cast<DiscreteDataType *>(r->value(groupingLabel2));

        DiscreteTypePointer category1Pointer(category1Value);
        DiscreteTypePointer category2Pointer(category2Value);

        innerCategories.insert(category2Pointer);

        MultiCategorizedSeries::iterator outerIterator =
                categorizedSeries.find(category1Pointer);

        if (outerIterator == categorizedSeries.end())
        {
            outerIterator = categorizedSeries
                    .insert(category1Pointer,
                            QSharedPointer<CategorizedSeries>(
                                new CategorizedSeries()));
        }

        CategorizedSeries *innerMap = outerIterator.value().data();
        CategorizedSeries::iterator innerIterator =
                innerMap->find(category2Pointer);

        if (innerIterator == innerMap->end())
        {
            innerIterator = innerMap
                    ->insert(category2Pointer,
                             QSharedPointer<QList<qreal> >(
                                 new QList<qreal>()));
        }

        innerIterator.value().data()->append(y);
    }

    return CategorizationResult(categorizedSeries, innerCategories);
}

void Chart2DManager::removeUnboxableCategories(MultiCategorizedSeries &cs,
                                               QSet<DiscreteTypePointer> &innerCats)
{

    QMap<DiscreteTypePointer, bool> innerCatsOccurs;
    for (const DiscreteTypePointer &p : innerCats)
    {
        innerCatsOccurs.insert(p, false);
    }

    MultiCategorizedSeries::key_value_iterator outerIter =
            cs.keyValueBegin();
    QList<DiscreteTypePointer> emptyOuterCats;

    for (; outerIter != cs.keyValueEnd(); outerIter++)
    {
        CategorizedSeries *innerMap = (*outerIter).second.data();
        bool outerCatOccurs = false;

        for (const DiscreteTypePointer &p : innerCats)
        {
            CategorizedSeries::iterator innerIter = innerMap->find(p);
            if (innerIter != innerMap->end())
            {
                QList<qreal> *data = innerIter.value().data();
                bool boxable = data->count() >= 4;
                bool *innerCatOccurs = &(*(innerCatsOccurs.find(p)));
                *innerCatOccurs = *innerCatOccurs || boxable;
                outerCatOccurs = outerCatOccurs || boxable;
            }
        }

        if (!outerCatOccurs)
        {
            emptyOuterCats.append((*outerIter).first);
        }
    }

    if (cs.keys().count() > 1)
    {
        for (const DiscreteTypePointer &p : emptyOuterCats)
        {
            cs.remove(p);
        }
    }

    if (innerCats.count() > 1)
    {
        QMap<DiscreteTypePointer, bool>::key_value_iterator iter =
                innerCatsOccurs.keyValueBegin();
        for (; iter != innerCatsOccurs.keyValueEnd(); iter++)
        {
            if (!(*iter).second)
            {
                innerCats.remove((*iter).first);
            }
        }
    }
}

ContinuousDataType *Chart2DManager::cont(DataType *value) const
{
    return dynamic_cast<ContinuousDataType *>(value);
}

qreal Chart2DManager::findMedian(QList<qreal> *values, int begin, int end) const
{
    int count = end - begin;
    if (count % 2) {
        return values->at(count / 2 + begin);
    } else {
        qreal right = values->at(count / 2 + begin);
        qreal left = values->at(count / 2 - 1 + begin);
        return (right + left) / 2.0;
    }
}

void Chart2DManager::scaleAxes()
{
    zoomAxis(xAxis, ui->xZoom->value());
    zoomAxis(yAxis, ui->yZoom->value());
}

void Chart2DManager::zoomAxis(AxisManager *axis, int percent)
{
    if (axis != nullptr)
    {
        axis->setZoom(qreal(percent) / 100.);
        axis->update();
    }
}

void Chart2DManager::clearLastChart()
{
    if (lastChart != nullptr)
    {
        lastChart->removeAllSeries();
        delete lastChart;
        lastChart = nullptr;
    }
}

QPointF Chart2DManager::pairToPoint(const QPair<qreal, qreal> &p)
{
    return QPointF(p.first, p.second);
}

qreal Chart2DManager::distance(const QPointF &p1, const QPointF &p2)
{
    QPointF diff = p1 - p2;
    double x = diff.x() * diff.x();
    double y = diff.y() * diff.y();
    return qSqrt(x + y);
}

void Chart2DManager::updateChartInfo(const QPointF &point)
{
    static QPointF lastClickedPoint = QPointF(123456789, 123456789);
    ui->chartInfo->clear();

    auto pointsIter = pointToRecord.keyBegin();
    QPointF closestPoint = pairToPoint(*pointsIter++);
    qreal closestDist = distance(point, closestPoint);

    for (; pointsIter != pointToRecord.keyEnd(); pointsIter++)
    {
        QPointF currPoint = pairToPoint(*pointsIter);
        qreal dist = distance(point, currPoint);
        if (dist < closestDist)
        {
            closestPoint = currPoint;
            closestDist = dist;
        }
    }

    if (lastClickedPoint == closestPoint)
    {
        lastClickedPoint = QPointF(123456789, 123456789);
    }
    else
    {
        lastClickedPoint = closestPoint;

        DataRecord *clickedRecord = pointToRecord.value(
                    QPair<qreal, qreal>(closestPoint.x(), closestPoint.y()));

        for (const QString &key : clickedRecord->keys())
        {
            QString keyValue = QString("'%1' = '%2'")
                    .arg(key).arg(clickedRecord->value(key)->toString());
            ui->chartInfo->addItem(keyValue);
        }
    }
}

void Chart2DManager::updateChartInfo(QBoxSet *boxSet)
{
    static QBoxSet *lastClickedSet = nullptr;
    ui->chartInfo->clear();

    if (lastClickedSet == boxSet)
    {
        lastClickedSet = nullptr;
    }
    else
    {
        lastClickedSet = boxSet;

        QMap<QString, QString> r = boxToRecord.value(boxSet);
        for (const QString &key : r.keys())
        {
            QString keyValue = QString("'%1' = '%2'")
                    .arg(key).arg(r.value(key));
            ui->chartInfo->addItem(keyValue);
        }

        ui->chartInfo->sortItems();
    }
}

void Chart2DManager::updateChartInfo(int index, QBarSet *barSet)
{
    static QBarSet *lastClickedSet = nullptr;
    static int lastIndex = -1;
    ui->chartInfo->clear();

    if (lastClickedSet == barSet && lastIndex == index)
    {
        lastClickedSet = nullptr;
        lastIndex = -1;
    }
    else
    {
        lastClickedSet = barSet;
        lastIndex = index;

        QMap<QString, QString> r = barSetToRecords.value(barSet).at(index);
        r.insert("'Liczba elementów'", QString::number(barSet->at(index)));

        for (const QString &key : r.keys())
        {
            QString keyValue = QString("'%1' = '%2'")
                    .arg(key).arg(r.value(key));
            ui->chartInfo->addItem(keyValue);
        }
    }
}

void Chart2DManager::zoomX(int percent)
{
    zoomAxis(xAxis, percent);
}

void Chart2DManager::zoomY(int percent)
{
    zoomAxis(yAxis, percent);
}
