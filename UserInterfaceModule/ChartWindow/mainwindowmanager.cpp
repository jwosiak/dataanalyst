/****************************************************************************
**
** Copyright (C) 2018  Jakub Wosiak
** Contact: jakubwosiak@gmail.com
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#include "mainwindowmanager.h"
#include "continuousdatatype.h"
#include "chart2dmanager.h"
#include "ui_chartwindow.h"

#include <QGroupBox>
#include <QVBoxLayout>
#include <QGridLayout>
#include <QSizePolicy>
#include <continuousintervaltype.h>
#include <attribgenwindow.h>
#include <databaseupdator.h>
#include <QSqlDatabase>
#include <QtDebug>

static ConfigReader *conf = ConfigReader::getInstance();
static GenericDataProvider *dataProvider = GenericDataProvider::getInstance();
static GenericFactory *factory = GenericFactory::getInstance();

static const QString attachedIntervalsLabelPattern = "Grupowanie_%1";

MainWindowManager::MainWindowManager(Ui::ChartWindow *ui,
                                       Chart2DManager *chartManager,
                                       QObject *parent)
    : QObject (parent)
    , chartManager(chartManager)
    , PAGE_SIZE(6)
    , ui(ui)
{    
    ui->elementsSortOrderSelect->addItem("malejąco", QVariant("DESC"));
    ui->elementsSortOrderSelect->addItem("rosnąco", QVariant("ASC"));

    ui->chartTypeSelect->addItem("Kartezjański");
    ui->chartTypeSelect->addItem("Pudełkowy");
    ui->chartTypeSelect->addItem("Słupkowy");

    ui->chartSubjectSelect->addItems(conf->selectCategories());

    QObject::connect(ui->chartSubjectSelect, SIGNAL(currentIndexChanged(int)),
                     this, SLOT(updateChartCategory(int)));

    QObject::connect(ui->elementsListSortSelect,
                     SIGNAL(currentIndexChanged(int)), this,
                     SLOT(updateElementsList()));

    QObject::connect(ui->elementsSortOrderSelect,
                     SIGNAL(currentIndexChanged(int)), this,
                     SLOT(updateElementsList()));

    QObject::connect(ui->pageIndexSelect, SIGNAL(valueChanged(int)), this,
                     SLOT(updateElementsList()));

    QObject::connect(ui->addFilterButton, SIGNAL(pressed()), this,
                     SLOT(addFilter()));

    QObject::connect(ui->removeFilterButton, SIGNAL(pressed()), this,
                     SLOT(removeFilter()));

    QObject::connect(ui->elementsList, SIGNAL(itemSelectionChanged()), this,
                     SLOT(updateChartSubject()));

    QObject::connect(ui->elementsList,
                     SIGNAL(itemDoubleClicked(QListWidgetItem*)), this,
                     SLOT(deselectCurrentElement(QListWidgetItem*)));

    QObject::connect(ui->xAxisSelect,
                     SIGNAL(currentIndexChanged(int)), this,
                     SLOT(updateChartSubject()));

    QObject::connect(ui->yAxisSelect,
                     SIGNAL(currentIndexChanged(int)), this,
                     SLOT(updateChartSubject()));

    QObject::connect(ui->chartTypeSelect, SIGNAL(currentIndexChanged(int)),
                     this, SLOT(updateChartType()));

    QObject::connect(ui->groupingSelect, SIGNAL(currentIndexChanged(int)),
                     this, SLOT(updateIntervalsNum()));
    QObject::connect(ui->groupingSelect2, SIGNAL(currentIndexChanged(int)),
                     this, SLOT(updateIntervalsNum()));

    QObject::connect(ui->saveButton, SIGNAL(pressed()), this,
                     SLOT(saveNextRecords()));

    QObject::connect(ui->unsaveButton, SIGNAL(pressed()), this,
                     SLOT(clearSavedRecords()));

    QObject::connect(ui->intervalNum, SIGNAL(valueChanged(int)), this,
                     SLOT(updateChartSubject()));
    QObject::connect(ui->intervalNum2, SIGNAL(valueChanged(int)), this,
                     SLOT(updateChartSubject()));

    QObject::connect(ui->attribGenButton, SIGNAL(pressed()), this,
                     SLOT(openAttribGenWindow()));

    QObject::connect(ui->resetButton, SIGNAL(pressed()), this,
                     SLOT(resetInterface()));

    QObject::connect(ui->reloadConfigButton, SIGNAL(pressed()), this,
                     SLOT(reloadConfig()));

    QObject::connect(ui->reopenDBButton, SIGNAL(pressed()), this,
                     SLOT(reopenDatabase()));

    resetInterface();
}

MainWindowManager::~MainWindowManager()
{
    delete root->layout();
    delete root;
}

QWidget *MainWindowManager::getRoot() const
{
    return root;
}

QList<QString> MainWindowManager::filtersToString()
{
    QList<QString> res;

    for (Record *filter : selectedFilters)
    {
        res.append(filter->value("condition").toString());
    }

    return res;
}

QString MainWindowManager::interpolateWithParams(const QString &s)
{
    QString res = s;
    QString param1 = ui->parameterField1->toPlainText();
    QString param2 = ui->parameterField2->toPlainText();

    if (param1.size() != 0)
    {
        res = res.arg(param1);
        if (param2.size() != 0)
        {
            res = res.arg(param2);
        }
    }

    return res;
}

void MainWindowManager::clearParams()
{
    ui->parameterField1->clear();
    ui->parameterField2->clear();
}

void MainWindowManager::updateAxesSelectors()
{
    ui->xAxisSelect->blockSignals(true);
    ui->yAxisSelect->blockSignals(true);
    ui->xAxisSelect->clear();
    ui->yAxisSelect->clear();
    QList<QMap<QString, QString> > axes = conf->axesConfig();
    const QString label = "label", type = "type";

    for (const QMap<QString, QString> &axis : axes)
    {
        const QString currentType = axis.value(type);
        const QString currentLabel = axis.value(label);

        if (factory->isContinuous(currentType))
        {
            ui->xAxisSelect->addItem(currentLabel, QVariant(currentType));
            ui->yAxisSelect->addItem(currentLabel, QVariant(currentType));
        }
    }

    if (ui->xAxisSelect->count() > 1)
    {
        ui->xAxisSelect->setCurrentIndex(1);
    }
    ui->xAxisSelect->blockSignals(false);
    ui->yAxisSelect->blockSignals(false);
}

void MainWindowManager::updateSavedList()
{
    ui->savedList->blockSignals(true);
    ui->savedList->clear();

    for (const NamedRecords &nr : savedRecords)
    {
        ui->savedList->addItem(nr.first);
    }
    ui->savedList->blockSignals(false);
}

void MainWindowManager::saveNextRecords()
{
    if (!currentRecords.first.isEmpty() && !areSaved(currentRecords.first))
    {
        savedRecords.append(currentRecords);
        currentRecords = NamedRecords("", QList<DataRecord *>());
    }
    updateSavedList();
}

void MainWindowManager::clearSavedRecords()
{
    ui->savedList->blockSignals(true);
    if (!ui->savedList->selectedItems().empty())
    {
        QList<int> toRemove;
        for (QListWidgetItem *selectedItem : ui->savedList->selectedItems())
        {
            int selectedId = ui->savedList->row(selectedItem);
            toRemove << selectedId;

            NamedRecords nr = savedRecords.at(selectedId);
            if (nr.first != currentRecords.first)
            {
                deleteRecords(nr.second);
            }
        }

        QList<NamedRecords>::iterator iter = savedRecords.begin();
        for (int i = 0; iter != savedRecords.end(); i++)
        {
            if (toRemove.contains(i))
            {
                iter = savedRecords.erase(iter);
            }
            else
            {
                iter++;
            }
        }

    }
    ui->savedList->blockSignals(false);
    updateSavedList();
    updateChartSubject();
}

void MainWindowManager::updateIntervalsNum()
{
    toggleIntervalNum(ui->groupingSelect, ui->intervalNum);
    toggleIntervalNum(ui->groupingSelect2, ui->intervalNum2);
    updateChartSubject();
}

void MainWindowManager::openAttribGenWindow()
{
    AttribGenWindow *w = new AttribGenWindow();
    w->show();
}

void MainWindowManager::reloadConfig()
{
    conf->loadConfigFile(conf->getPath());
    resetInterface();
}

void MainWindowManager::reopenDatabase()
{
    QSqlDatabase *sqlDatabase = SqlCommunicator::connectWithDB();
    dataProvider->setDatabase(sqlDatabase);
    DatabaseUpdator::getInstance()->setDatabase(sqlDatabase);
    resetInterface();
}

QPair<Record *, Record *> MainWindowManager::groupedBy()
{
    QVariant currentData1 = ui->groupingSelect->currentData(Qt::UserRole);
    QVariant currentData2 = ui->groupingSelect2->currentData(Qt::UserRole);
    return QPair<Record *, Record *>(
                dataProvider->recordFromVariant(&currentData1),
                dataProvider->recordFromVariant(&currentData2));
}

bool MainWindowManager::areSaved(QString recordsName)
{
    for (NamedRecords &nr : savedRecords)
    {
        if (nr.first == recordsName)
        {
            return true;
        }
    }

    return false;
}

void MainWindowManager::deleteRecords(QList<DataRecord *> &records)
{
    while (!records.isEmpty())
    {
        DataRecord *record = records.first();
        records.pop_front();

        for (QString &key : record->keys())
        {
            DataType *value = record->value(key);
            record->remove(key);
            delete value;
        }

        delete record;
    }
}

void MainWindowManager::updateCurrentRecords()
{
    QList<QListWidgetItem *> selectedItems = ui->elementsList->selectedItems();
    if (!selectedItems.isEmpty())
    {
        QListWidgetItem *selectedItem = selectedItems.first();
        QString newlySelectedName = selectedItem->text();
        bool saved = areSaved(currentRecords.first);

        if (newlySelectedName != currentRecords.first)
        {
            if (!saved)
            {
                deleteRecords(currentRecords.second);
            }

            if (!areSaved(newlySelectedName))
            {
                QString setPointer =
                        selectedItem->data(Qt::UserRole).toString();
                QList<DataRecord *> records =
                        dataProvider->selectData(setPointer, newlySelectedName);
                currentRecords = NamedRecords(selectedItem->text(), records);
            }
            else
            {
                currentRecords = NamedRecords("", QList<DataRecord *>());
            }
        }
    }
    else
    {
        if (!currentRecords.second.isEmpty())
        {
            deleteRecords(currentRecords.second);
            currentRecords = NamedRecords("", QList<DataRecord *>());
        }
    }
}

QList<DataRecord *> MainWindowManager::allRecords()
{
    QList<DataRecord *> result;

    for (const NamedRecords &nr : savedRecords)
    {
        result.append(nr.second);
    }
    result.append(currentRecords.second);

    return result;
}

Record *MainWindowManager::attachIntervals(QList<DataRecord *> &records,
                                            Record* grouper, int id,
                                            int intervCount)
{
    Record *result = new Record();
    GenericFactory *fact = GenericFactory::getInstance();

    const QString intervId = QString(attachedIntervalsLabelPattern).arg(id);
    const QString type = grouper->value("type").toString();
    const QString label = grouper->value("label").toString();
    DataTypeCons cons = fact->getInstance()->getCons(type);

    std::sort(records.begin(), records.end(),
          [&label](DataRecord *r1, DataRecord *r2)
    { return *dynamic_cast<ContinuousDataType *>(r1->value(label))
                < *dynamic_cast<ContinuousDataType *>(r2->value(label)); });

    qreal min = dynamic_cast<ContinuousDataType *>(
                records.first()->value(label))->toReal();
    qreal max = dynamic_cast<ContinuousDataType *>(
                records.last()->value(label))->toReal();

    QVector<ContinuousIntervalType *> allIntervals(intervCount);
    qreal intervWidth = (max - min) / intervCount;
    intervWidth = intervWidth > 0? intervWidth : 1;
    DataRecord *anyRecord = records.first();
    ContinuousDataType *anyValue =
            dynamic_cast<ContinuousDataType *>(anyRecord->value(label));

    for (int i = 0; i < intervCount; i++)
    {
        ContinuousDataType *left = anyValue->fromReal(min + intervWidth * i);
        ContinuousDataType *right = anyValue->fromReal(min + intervWidth * (i+1));
        allIntervals[i] = new ContinuousIntervalType(type, left, right);
    }

    for (DataRecord *r : records)
    {
        qreal val = dynamic_cast<ContinuousDataType *>(r->value(label))->toReal();
        int interv = int((val-min) / intervWidth);
        interv = interv >= intervCount? intervCount-1 : interv;
        r->insert(intervId, new ContinuousIntervalType(*allIntervals[interv]));
    }

    result->insert("label", QVariant(intervId));
    result->insert("type", "continuous interval");

    while (!allIntervals.isEmpty())
    {
        ContinuousIntervalType *interv = allIntervals.last();
        allIntervals.pop_back();
        delete interv;
    }

    return result;
}

void MainWindowManager::updateGroupingSelect(QComboBox *groupingSelect)
{
    groupingSelect->blockSignals(true);
    groupingSelect->clear();
    groupingSelect->addItem("Brak",
                            dataProvider->recordToVariant(new Record()));

    QList<Record *> items = conf->selectGroupedBy();
    items.append(conf->selectAxes());
    for (Record *r : items)
    {
        groupingSelect->addItem(r->value("label").toString(),
                                dataProvider->recordToVariant(r));
    }
    Record *specialCase = new Record();
    specialCase->insert("type", "string");
    specialCase->insert("label", "Nazwa elementu");
    specialCase->insert("column", "built_in_column_element_name");

    groupingSelect->addItem("Nazwa elementu",
                            dataProvider->recordToVariant(specialCase));
    groupingSelect->blockSignals(false);
}

void MainWindowManager::toggleIntervalNum(QComboBox *groupingSelect, QSpinBox *intervalNum)
{
    int groupCount = groupingSelect->count();
    intervalNum->setEnabled(groupCount > 0);
    if (groupCount > 0)
    {
        QVariant selectedItem = groupingSelect->currentData();
        QString type = dataProvider->recordFromVariant(&selectedItem)
                ->value("type").toString();
        intervalNum->setEnabled(factory->isContinuous(type));
    }
}

bool MainWindowManager::recentlyAttachedIntervals(QList<DataRecord *> &records,
                                                   int groupId)
{
    if (!records.isEmpty())
    {
        QString groupName =
                QString(attachedIntervalsLabelPattern).arg(groupId);
        return records.first()->contains(groupName);
    }
    return false;
}

void MainWindowManager::clearUnusedAttachedIntervals(QList<DataRecord *> &records,
                                                      int groupId)
{
    QString groupName = QString(attachedIntervalsLabelPattern).arg(groupId);

    for (DataRecord *r : records)
    {
        DataRecord::iterator iter = r->find(groupName);
        DataType *unusedData = iter.value();
        r->erase(iter);
        delete unusedData;
    }
}

void MainWindowManager::addFilter()
{   
    if (!ui->filtersList->selectedItems().empty())
    {
        QListWidgetItem *selected = ui->filtersList->selectedItems().first();

        Record *filter = new Record(*dataProvider->unpackRecord(selected));
        filter->insert("name", interpolateWithParams(
                           filter->value("name").toString()));
        filter->insert("condition", interpolateWithParams(
                           filter->value("condition").toString()));

        selectedFilters.append(filter);
    }

    clearParams();
    updateSelectedFilters();

    updateMaxPage();
    updateElementsList();
}

void MainWindowManager::removeFilter()
{
    if (!ui->selectedFilters->selectedItems().empty())
    {
        QListWidgetItem *selected =
                ui->selectedFilters->selectedItems().first();

        selectedFilters.removeAll(dataProvider->unpackRecord(selected));

        updateSelectedFilters();

        updateMaxPage();
        updateElementsList();
    }
}

void MainWindowManager::updateChartCategory(int category)
{
    chartCategory = category;
    ui->elementsList->clear();
    updateElementsListSorter();
    selectedFilters.clear();
    ui->selectedFilters->clear();
    updateFiltersList();
    ui->pageIndexSelect->setValue(1);
    updateMaxPage();
    updateElementsList();
}

void MainWindowManager::resetInterface()
{
    updateChartCategory(0);

    if (!areSaved(currentRecords.first))
    {
        deleteRecords(currentRecords.second);
    }

    currentRecords = NamedRecords("", QList<DataRecord *>());

    while (!savedRecords.isEmpty())
    {
        NamedRecords nr = savedRecords.first();
        savedRecords.pop_front();
        deleteRecords(nr.second);
    }
    ui->savedList->clear();

    updateChartType();

    updateAxesSelectors();

    updateGroupingSelects();

    updateElementsListSorter();

    ui->intervalNum->setValue(5);
    ui->intervalNum2->setValue(5);
    updateIntervalsNum();
    updateElementsList();

    ui->chartSubjectSelect->setCurrentIndex(0);
}

void MainWindowManager::updateMaxPage()
{
    int elements = dataProvider->selectElementsCount(filtersToString(),
                                                     chartCategory);
    maxPageNumber = elements / PAGE_SIZE + 1;
    ui->pageIndexLabel->setText(QString("z %1").arg(maxPageNumber));
    ui->pageIndexSelect->setMaximum(maxPageNumber);
}

void MainWindowManager::updateChartType()
{
    bool isCartesian = ui->chartTypeSelect->currentText() == "Kartezjański";
    ui->xAxisSelect->setEnabled(isCartesian);
    ui->groupingSelect2->setEnabled(!isCartesian);

    if (ui->groupingSelect2->isEnabled())
    {
        toggleIntervalNum(ui->groupingSelect2, ui->intervalNum2);
    }
    else
    {
        ui->intervalNum2->setEnabled(false);
    }

    ui->xZoom->setEnabled(isCartesian);

    ui->yAxisSelect->setEnabled(
                ui->chartTypeSelect->currentText() != "Słupkowy");


    updateChartSubject();
}

void MainWindowManager::updateSelectedFilters()
{
    ui->selectedFilters->clear();

    for (Record *filter : selectedFilters)
    {
        QListWidgetItem *i =
                dataProvider->packRecord(filter->value("name").toString(),
                                         filter);
        ui->selectedFilters->addItem(i);

    }
}

void MainWindowManager::updateFiltersList()
{
    ui->filtersList->clear();

    QList<Record*> filters = conf->selectFilters(chartCategory);

    for (Record *r : filters)
    {
        ui->filtersList->addItem(dataProvider->packRecord(
                                 r->value("name").toString(), r));
    }
}

void MainWindowManager::updateElementsListSorter()
{
    ui->elementsListSortSelect->blockSignals(true);
    ui->elementsListSortSelect->clear();

    AliasMap namesMap = dataProvider->columnsNamesAliases(chartCategory).second;
    for (const QString &key : namesMap.keys())
    {
        ui->elementsListSortSelect->addItem(namesMap.value(key),
                                            QVariant(key));
    }

    ui->elementsListSortSelect->blockSignals(false);
}

void MainWindowManager::updateElementsList()
{
    QString orderBy = ui->elementsListSortSelect->currentData().toString();
    ui->elementsList->clear();

    if (orderBy.isEmpty())
    {
        return;
    }

    QString sortOrder = ui->elementsSortOrderSelect->currentData().toString();
    int pageNumber = ui->pageIndexSelect->value()-1;

    ElementsList el = dataProvider->selectElements(filtersToString(),
                                                   orderBy, sortOrder,
                                                   pageNumber*PAGE_SIZE,
                                                   PAGE_SIZE, chartCategory);

    ui->elementsHeader->setText(el.first);

    for (QPair<QString, QString> &element : el.second)
    {
        QListWidgetItem *item = new QListWidgetItem();
        item->setText(element.first);
        item->setData(Qt::UserRole, QVariant(element.second));
        ui->elementsList->addItem(item);
    }

    updateChartSubject();
}

void MainWindowManager::deselectCurrentElement(QListWidgetItem *item)
{
    if (item->isSelected())
    {
        item->setSelected(false);
        updateChartSubject();
    }
}

void MainWindowManager::updateGroupingSelects()
{
    updateGroupingSelect(ui->groupingSelect);
    updateGroupingSelect(ui->groupingSelect2);
}

void MainWindowManager::updateChartSubject()
{
    updateCurrentRecords();
    QList<DataRecord *> passedRecords = allRecords();
    ui->chartInfo->clear();

    if (!passedRecords.isEmpty())
    {
        QPair<Record *, Record *> groupers = groupedBy();

        if (factory->isContinuous(groupers.first->value("type").toString()))
        {
            groupers.first = attachIntervals(passedRecords, groupers.first, 1,
                                             ui->intervalNum->value());
        }
        else if (recentlyAttachedIntervals(passedRecords, 1))
        {
            clearUnusedAttachedIntervals(passedRecords, 1);
        }

        if (factory->isContinuous(groupers.second->value("type").toString()))
        {
            groupers.second = attachIntervals(passedRecords, groupers.second, 2,
                                              ui->intervalNum2->value());
        }
        else if (recentlyAttachedIntervals(passedRecords, 2))
        {
            clearUnusedAttachedIntervals(passedRecords, 2);
        }

        if (ui->chartTypeSelect->currentText() == "Kartezjański")
        {
            chartManager->drawCartesianChart(
                        passedRecords, groupers.first,
                        ui->xAxisSelect->currentText(),
                        ui->yAxisSelect->currentText(),
                        ui->xAxisSelect->currentData().toString(),
                        ui->yAxisSelect->currentData().toString());
        }
        if (ui->chartTypeSelect->currentText() == "Pudełkowy")
        {
            chartManager->drawBoxChart(
                        passedRecords, groupers.first,
                        groupers.second,
                        ui->yAxisSelect->currentText(),
                        ui->yAxisSelect->currentData().toString());
        }
        if (ui->chartTypeSelect->currentText() == "Słupkowy")
        {
            chartManager->drawBarChart(
                        passedRecords, groupers.first, groupers.second,
                        ui->yAxisSelect->currentText());
        }

    }
    else
    {
        chartManager->drawBlankChart();
    }
}
