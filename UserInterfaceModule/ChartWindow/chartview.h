/****************************************************************************
**
** Copyright (C) 2018  Jakub Wosiak
** Contact: jakubwosiak@gmail.com
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#ifndef CHARTVIEW_H
#define CHARTVIEW_H

#include <QChart>
#include <QChartView>
#include "axismanager.h"

namespace Ui{
class ChartWindow;
}

class QPointF;
class QSpinBox;

QT_CHARTS_USE_NAMESPACE

class ChartView : public QChartView
{
    Q_OBJECT

    AxisManager *xAxis;
    AxisManager *yAxis;

    QPointF lastMousePos;
    bool mousePressed = false;
    Ui::ChartWindow *ui;

public:
    ChartView(QWidget *parent = 0);
    ChartView(QChart* chart, QWidget *parent = 0);
    ChartView(Ui::ChartWindow *ui, QChart* chart, QWidget *parent = 0);

    AxisManager *getXAxis() const;
    void setXAxis(AxisManager *value);

    AxisManager *getYAxis() const;
    void setYAxis(AxisManager *value);

    Ui::ChartWindow *getUi() const;
    void setUi(Ui::ChartWindow *value);

protected:
    virtual void mousePressEvent(QMouseEvent *event);
    virtual void mouseReleaseEvent(QMouseEvent *event);
    virtual void mouseMoveEvent(QMouseEvent *event);
    virtual void wheelEvent(QWheelEvent *event);

private:
    void zoomSingleAxis(bool direction, AxisManager *axis, QSpinBox *zoomBox);
    void updateZoom(AxisManager *axis, int percent);

};

#endif // CHARTVIEW_H
