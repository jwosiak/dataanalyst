/****************************************************************************
**
** Copyright (C) 2018  Jakub Wosiak
** Contact: jakubwosiak@gmail.com
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#include "configreader.h"
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonDocument>
#include <QFile>
#include <QTextStream>

#include <QtDebug>

void ConfigReader::loadConfigFile(QString fileName)
{
    path = fileName;
    QFile file(fileName);

    file.open(QIODevice::ReadOnly);

    if (file.isOpen())
    {
        qDebug() << "Opened" << fileName;
        QTextStream fileTextStream(&file);
        QJsonDocument doc =
                QJsonDocument::fromJson(fileTextStream.readAll().toUtf8());

        if (doc.isNull())
        {
            qDebug() << "Config error: " << fileName
                     << "has not a valid JSON format!";
        }
        else
        {
            hasValidRoot = true;
        }

        configRoot = new QJsonObject(doc.object());
    }
    else
    {
        qDebug() << "Failed to open" << fileName;
        configRoot = new QJsonObject();
    }
}

ConfigReader *ConfigReader::getInstance()
{
    static ConfigReader *instance = new ConfigReader();

    return instance;
}

QJsonObject *ConfigReader::getConfigRoot() const
{
    return configRoot;
}

QJsonValue ConfigReader::value(const QJsonObject &obj, const QString &key)
{
    QJsonValue val = obj.value(key);
    if (hasValidRoot && val.isUndefined())
    {
        QStringList possibleKeys = obj.keys();
        for (QString &s : possibleKeys)
        {
            s = QString("'%1'").arg(s);
        }

        if (!possibleKeys.isEmpty())
        {
            qDebug() << "Config error: Key" << key << "not found in object!"
                     << QString("Possible keys: %1").arg(possibleKeys.join(", "));
        }
        else
        {
            qDebug() << "Config error: Key" << key << "not found in object!";
        }
    }

    return val;
}


QJsonArray ConfigReader::chartCategoriesArray()
{
    return value(*getConfigRoot(), "categories").toArray();
}

QString ConfigReader::getBaseTableName(int category)
{
    return value(baseTable(category), "name").toString();
}

QString ConfigReader::getBaseTableUniqueId(int category)
{
    return value(baseTable(category), "unique id").toString();
}

QList<Record *> ConfigReader::selectRelatives(int category)
{
    QList<Record *> result;
    QJsonArray relatives =
            value(chartCategoriesArray()[category].toObject(),
                  "relatives").toArray();

    for (int i = 0; i < relatives.count(); i++)
    {
        QJsonObject current = relatives.at(i).toObject();
        result.append(new Record(current.toVariantMap()));
    }

    return result;
}

int ConfigReader::getCategory(QString profileName)
{
    QJsonArray charts = chartCategoriesArray();

    for (int i = 0; i < charts.count(); i++)
    {
        if (value(charts.at(i).toObject(), "name").toString() == profileName)
        {
            return i;
        }
    }

    return -1;
}

QJsonObject ConfigReader::searchConfig(int category)
{
    QJsonArray charts = chartCategoriesArray();
    return value(charts.at(category).toObject(), "search").toObject();
}

QJsonObject ConfigReader::chartsConfig()
{
    return value(*configRoot, "items").toObject();
}

QList<Record *> ConfigReader::selectFilters(int category)
{
    QList<Record*> result;

    QJsonObject search = searchConfig(category);
    QJsonArray filtersArray = value(search, "filters").toArray();

    for (int i = 0; i < filtersArray.count(); i++)
    {
        Record *r = new Record(filtersArray.at(i).toObject().toVariantMap());
        result.append(r);
    }

    return result;
}

QList<QString> ConfigReader::selectAxesTypes()
{
    QList<QString> res;
    QJsonArray axesTypes = value(chartsConfig(), "axes").toArray();

    const QString label = "label";

    for (int i = 0; i < axesTypes.count(); i++)
    {
        QJsonObject axisConfig = axesTypes.at(i).toObject();
        res.append(value(axisConfig, label).toString());
    }

    return res;
}

QList<Record *> ConfigReader::selectGroupedBy()
{
    QList<Record *> res;
    QJsonArray a = value(chartsConfig(), "grouped by").toArray();
    for (int i = 0; i < a.count(); i++)
    {
        QJsonObject current = a.at(i).toObject();
        res.append(new Record(current.toVariantMap()));
    }
    return res;
}

QList<Record *> ConfigReader::selectAxes()
{
    QList<Record *> res;
    QJsonArray a = value(chartsConfig(), "axes").toArray();
    for (int i = 0; i < a.count(); i++)
    {
        QJsonObject current = a.at(i).toObject();
        res.append(new Record(current.toVariantMap()));
    }
    return res;
}

QList<QString> ConfigReader::selectCategories()
{
    QList<QString> res;

    QJsonArray charts = chartCategoriesArray();
    for (int i = 0; i < charts.count(); i++)
    {
        res.append(value(charts.at(i).toObject(), "name").toString());
    }

    return res;
}


static const QString lab = "label";

QString ConfigReader::axisType(const QString &label)
{
    QJsonArray axesConfigs = axes();

    for (int i = 0; i < axesConfigs.count(); i++)
    {
        QJsonObject elem = axesConfigs.at(i).toObject();
        if (value(elem, lab).toString() == label)
        {
            return value(elem, "type").toString();
        }
    }

    return "string";
}

QString ConfigReader::axisColumn(const QString &label)
{
    QJsonArray axesConfigs = axes();

    for (int i = 0; i < axesConfigs.count(); i++)
    {
        QJsonObject elem = axesConfigs.at(i).toObject();
        if (value(elem, lab).toString() == label)
        {
            return value(elem, "column").toString();
        }
    }

    return "";
}

QJsonObject ConfigReader::attribGen()
{
    return value(chartsConfig(), "attribute generation").toObject();
}

QList<QMap<QString, QString> > ConfigReader::typesConfig(const QJsonObject &bindingConf,
                                                         QString arrayName)
{
    QList<QMap<QString, QString> > result;
    QJsonArray typesConfigs = value(bindingConf, arrayName).toArray();
    QList<QString> keys;
    keys << "column" << "label" << "type";

    for (int i = 0; i < typesConfigs.count(); i++)
    {
        QJsonObject elem = typesConfigs.at(i).toObject();
        QMap<QString, QString> axisConfig;

        for (QString key : elem.keys())
        {
            if (!keys.contains(key))
            {
                qDebug() << QString("Config error: Unrecognized key '%1' at %2%3")
                            .arg(key).arg("categories->items->").arg(arrayName);
            }
            axisConfig.insert(key, value(elem, key).toString());
        }

        result.append(axisConfig);
    }

    return result;
}

QList<QMap<QString, QString> > ConfigReader::axesConfig()
{
    QList<QMap<QString, QString> > result;
    QJsonArray axesConfigs =
            value(chartsConfig(), "axes").toArray();

    QList<QString> keys;
    keys << "column" << "label" << "type";

    for (int i = 0; i < axesConfigs.count(); i++)
    {
        QJsonObject elem = axesConfigs.at(i).toObject();
        QMap<QString, QString> axisConfig;

        for (QString key : elem.keys())
        {
            if (!keys.contains(key))
            {
                qDebug() << QString("Config error: Unrecognized key '%1' at %2")
                            .arg(key).arg("categories->items->axes");
            }
            axisConfig.insert(key, value(elem, key).toString());
        }

        result.append(axisConfig);
    }

    return result;
}

QString ConfigReader::dataQueryCondition(const QJsonObject &bindingConf, Record *ids)
{
    QJsonArray passedIds = value(bindingConf, "passed ids").toArray();
    QString wherePattern =
            value(bindingConf, "where condition").toString();

    for (int i = 0; i < passedIds.count(); i++)
    {
        QString idName = passedIds.at(i).toString();
        wherePattern = wherePattern.arg(ids->value(idName).toString());
    }

    return wherePattern;
}

QString ConfigReader::joinProfilesCondition(Record *relation)
{
    QString thisProfileId = relation->value("my id").toString();
    QString subjectProfileId = relation->value("referenced id").toString();
    bool eq = thisProfileId == subjectProfileId;
    return eq? QString("USING (%1)").arg(thisProfileId)
             : QString("ON (%1 = %2)").arg(thisProfileId).arg(subjectProfileId);
}

void ConfigReader::insertNewAxis(QString column, QString type,
                                 QString label)
{
    QJsonObject newAxis;
    newAxis.insert("column", column);
    newAxis.insert("type", type);
    newAxis.insert("label", label);

    configRoot->value("");

    QJsonValueRef itemsRef = (*getConfigRoot())["items"];
    QJsonObject newItems = itemsRef.toObject();
    QJsonValueRef axesRef = newItems["axes"];
    QJsonArray newAxes = axesRef.toArray();
    newAxes.append(newAxis);

    axesRef = newAxes;
    itemsRef = newItems;
}


ConfigReader::ConfigReader()
{

}

QString ConfigReader::getPath() const
{
    return path;
}

QJsonArray ConfigReader::axes()
{
    QJsonObject bindingConf = chartsConfig();
    return value(bindingConf, "axes").toArray();
}

QJsonObject ConfigReader::baseTable(int category)
{
    QJsonArray charts = chartCategoriesArray();
    return value(charts.at(category).toObject(), "base table").toObject();
}

QMap<QString, bool> ConfigReader::passedIds(const QJsonArray &pi)
{
    QMap<QString, bool> result;
    for (int i = 0; i < pi.count(); i++)
    {
        QJsonObject obj = pi.at(i).toObject();
        result.insert(obj.value("column").toString(),
                      obj.value("text type").toBool());
    }
    return result;
}

AliasMap ConfigReader::passedIdsAliases(const QJsonArray &pi)
{
    AliasMap result;
    for (int i = 0; i < pi.count(); i++)
    {
        QJsonObject obj = pi.at(i).toObject();
        result.insert(obj.value("column").toString(),
                      obj.value("destination column").toString());
    }
    return result;
}

QStringList ConfigReader::itemsPassedIds()
{
    QJsonArray idsArray =
                value(attribGen(), "passed ids").toArray();

    QStringList columns;

    for (int i = 0; i < idsArray.count(); i++)
    {
        QJsonObject obj = idsArray.at(i).toObject();
        columns << obj.value("destination column").toString();
    }

    return columns;
}
