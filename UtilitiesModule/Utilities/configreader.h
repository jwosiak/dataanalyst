/****************************************************************************
**
** Copyright (C) 2018  Jakub Wosiak
** Contact: jakubwosiak@gmail.com
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#ifndef CONFIGREADER_H
#define CONFIGREADER_H

#include <QList>
#include <QVariant>
#include <QMap>

class QJsonObject;
class QJsonArray;

typedef QMap<QString, QVariant> Record;
typedef QMap<QString, QString> AliasMap;

class ConfigReader
{
public:
    void loadConfigFile(QString fileName);

    static ConfigReader *getInstance();
    QJsonObject *getConfigRoot() const;

    QJsonValue value(const QJsonObject &obj, const QString &key);

    QJsonArray chartCategoriesArray();
    QString getBaseTableName(int category);
    QString getBaseTableUniqueId(int category);
    QList<Record *> selectRelatives(int category);
    int getCategory(QString profileName);
    QJsonObject searchConfig(int category);
    QJsonObject chartsConfig();
    QList<Record*> selectFilters(int category);
    QList<QString> selectAxesTypes();
    QList<Record *> selectGroupedBy();
    QList<Record *> selectAxes();
    QList<QString> selectCategories();
    QString axisType(const QString &label);
    QString axisColumn(const QString &label);
    QJsonObject attribGen();

    QList<QMap<QString, QString>> typesConfig(const QJsonObject &bindingConf,
                                              QString arrayName);
    QList<QMap<QString, QString>> axesConfig();
    QString dataQueryCondition(const QJsonObject &bindingConf, Record *ids);

    QString joinProfilesCondition(Record *relation);

    void insertNewAxis(QString column, QString type,
                       QString label);
    QString getPath() const;  

    QMap<QString, bool> passedIds(const QJsonArray &pi);
    AliasMap passedIdsAliases(const QJsonArray &pi);

    QStringList itemsPassedIds();
private:
    ConfigReader();
    QJsonObject *configRoot;
    QString path;
    bool hasValidRoot = false;

    QJsonArray axes();
    QJsonObject baseTable(int category);
};

#endif // CONFIGREADER_H
