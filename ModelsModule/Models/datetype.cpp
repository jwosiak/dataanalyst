/****************************************************************************
**
** Copyright (C) 2018  Jakub Wosiak
** Contact: jakubwosiak@gmail.com
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#include "datetype.h"

QDateTime DateType::getValue() const
{
    return value;
}

ContinuousDataType *DateType::fromReal(qreal value) const
{
    return new DateType(dateFromReal(value));
}

DateType::DateType()
{

}

DateType::DateType(QDateTime value)
    : value(value)
{

}

DateType::DateType(qreal value)
    : value(DateType::dateFromReal(value).getValue())
{

}

QString DateType::toString() const
{
    return value.toString("dd-MM-yyyy");
}

qreal DateType::toReal() const
{
    return value.toSecsSinceEpoch();
}

DateType DateType::dateFromReal(qreal value)
{
    QDateTime result =
            QDateTime::fromSecsSinceEpoch(static_cast<qint64>(value));
    return DateType(result);
}

QString DateType::format(qreal value)
{
    return dateFromReal(value).toString();
}
