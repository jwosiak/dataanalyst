/****************************************************************************
**
** Copyright (C) 2018  Jakub Wosiak
** Contact: jakubwosiak@gmail.com
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#include "timertype.h"

QTime TimerType::getValue() const
{
    return value;
}

ContinuousDataType *TimerType::fromReal(qreal value) const
{
    return new TimerType(timerFromReal(value));
}

TimerType::TimerType()
{

}

TimerType::TimerType(QTime value)
    : value(value)
{

}

TimerType::TimerType(qreal value)
    : value(TimerType::timerFromReal(value).getValue())
{

}

QString TimerType::toString() const
{
    return value.toString(QString("h:mm:ss"));
}

qreal TimerType::toReal() const
{
    return value.hour() * 3600 + value.minute() * 60 + value.second();
}

TimerType TimerType::timerFromReal(qreal value)
{
    int h, m, s;
    int seconds = static_cast<int>(value);

    h = seconds / 3600;
    m = (seconds % 3600) / 60;
    s = seconds % 60;

    return TimerType(QTime(h, m, s));
}

QString TimerType::format(qreal value)
{
    return timerFromReal(value).toString();
}
