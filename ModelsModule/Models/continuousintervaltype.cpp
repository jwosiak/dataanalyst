/****************************************************************************
**
** Copyright (C) 2018  Jakub Wosiak
** Contact: jakubwosiak@gmail.com
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#include "continuousintervaltype.h"
#include "genericfactory.h"

ContinuousDataType *ContinuousIntervalType::getLeft() const
{
    return left.data();
}

ContinuousDataType *ContinuousIntervalType::getRight() const
{
    return right.data();
}

ContinuousIntervalType::ContinuousIntervalType(QString type,
                                               ContinuousDataType *left,
                                               ContinuousDataType *right)
    : left(QSharedPointer<ContinuousDataType>(left))
    , right(QSharedPointer<ContinuousDataType>(right))
    , type(type)
{

}

ContinuousIntervalType::ContinuousIntervalType(const ContinuousIntervalType &interval)
    : left(interval.left)
    , right(interval.right)
    , type(interval.type)
{

}


QString ContinuousIntervalType::toString() const
{
    GenericFactory *fact = GenericFactory::getInstance();
    LabelFormatter format = fact->getFormat(type);
    return QString("%1 - %2")
            .arg(format(left.data()->toReal()))
            .arg(format(right.data()->toReal()));
}

bool ContinuousIntervalType::operator <(const DiscreteDataType &a) const
{
    const ContinuousIntervalType *b =
            dynamic_cast<const ContinuousIntervalType *>(&a);
    return left.data()->toReal() < b->getLeft()->toReal();
}
