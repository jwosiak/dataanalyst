/****************************************************************************
**
** Copyright (C) 2018  Jakub Wosiak
** Contact: jakubwosiak@gmail.com
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#include "genericfactory.h"

void GenericFactory::addItem(const QString &name, DataTypeCons value,
                             LabelFormatter formatter)
{
    namedConstructors.insert(name, value);
    namedFormatters.insert(name, formatter);
}

void GenericFactory::addItem(const QString &name, DataTypeCons value)
{
    namedConstructors.insert(name, value);
}

void GenericFactory::addItem(const QString &name, LabelFormatter formatter)
{
    namedFormatters.insert(name, formatter);
}

DataTypeCons GenericFactory::getCons(const QString &name)
{
    return namedConstructors.value(name);
}

LabelFormatter GenericFactory::getFormat(const QString &name)
{
    return namedFormatters.value(name);
}

bool GenericFactory::containsCons(const QString &name)
{
    return namedConstructors.contains(name);
}

bool GenericFactory::containsForm(const QString &name)
{
    return namedFormatters.contains(name);
}

void GenericFactory::registerDiscreteType(const QString &name)
{
    discreteTypes.append(name);
}

void GenericFactory::registerContinuousType(const QString &name)
{
    continuousTypes.append(name);
}

bool GenericFactory::isDiscrete(const QString &name)
{
    return discreteTypes.contains(name);
}

bool GenericFactory::isContinuous(const QString &name)
{
    return continuousTypes.contains(name);
}

GenericFactory::GenericFactory()
{

}

GenericFactory *GenericFactory::getInstance()
{
    static GenericFactory* instance = new GenericFactory();

    return instance;
}
