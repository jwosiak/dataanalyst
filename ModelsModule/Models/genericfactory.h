/****************************************************************************
**
** Copyright (C) 2018  Jakub Wosiak
** Contact: jakubwosiak@gmail.com
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#ifndef GENERICFACTORY_H
#define GENERICFACTORY_H

#include <QMap>
#include <functional>
#include <QPair>
#include "datatype.h"

class DataType;
class QVariant;

typedef std::function<DataType*(QVariant)> DataTypeCons;
typedef std::function<QString(qreal)> LabelFormatter;

class GenericFactory
{
private:
    QMap<QString, DataTypeCons> namedConstructors;
    QMap<QString, LabelFormatter> namedFormatters;
    QList<QString> discreteTypes;
    QList<QString> continuousTypes;

    GenericFactory();

public:
    static GenericFactory *getInstance();

    void addItem(const QString &name, DataTypeCons value,
                 LabelFormatter formatter);
    void addItem(const QString &name, DataTypeCons value);
    void addItem(const QString &name, LabelFormatter formatter);

    DataTypeCons getCons(const QString &name);
    LabelFormatter getFormat(const QString &name);
    bool containsCons(const QString &name);
    bool containsForm(const QString &name);

    void registerDiscreteType(const QString &name);
    void registerContinuousType(const QString &name);

    bool isDiscrete(const QString &name);
    bool isContinuous(const QString &name);
};

#endif // GENERICFACTORY_H
