/****************************************************************************
**
** Copyright (C) 2018  Jakub Wosiak
** Contact: jakubwosiak@gmail.com
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#ifndef DISCRETETYPEPOINTER_H
#define DISCRETETYPEPOINTER_H

#include <QHash>
#include "discretedatatype.h"

class DiscreteTypePointer
{
    DiscreteDataType *value;

public:
    DiscreteTypePointer(DiscreteDataType *value);
    DiscreteDataType *getValue() const;
};

inline bool operator==(const DiscreteTypePointer &a,
                       const DiscreteTypePointer &b)
{
    return *a.getValue() == *b.getValue();
}


inline bool operator<(const DiscreteTypePointer &a,
                      const DiscreteTypePointer &b)
{
    return *a.getValue() < *b.getValue();
}

inline uint qHash(const DiscreteTypePointer &a)
{
    return qHash(a.getValue()->toString());
}

#endif // DISCRETETYPEPOINTER_H
