/****************************************************************************
**
** Copyright (C) 2018  Jakub Wosiak
** Contact: jakubwosiak@gmail.com
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#ifndef DATETYPE_H
#define DATETYPE_H

#include "continuousdatatype.h"
#include <QDateTime>

class DateType : public ContinuousDataType
{
private:
    QDateTime value;

public:
    DateType();
    DateType(QDateTime value);
    DateType(qreal value);

public:
    virtual QString toString() const;
    virtual qreal toReal() const;

    static DateType dateFromReal(qreal value);
    static QString format(qreal value);
    QDateTime getValue() const;
    virtual ContinuousDataType *fromReal(qreal value) const;
};

#endif // DATETYPE_H
