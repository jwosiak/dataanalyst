/****************************************************************************
**
** Copyright (C) 2018  Jakub Wosiak
** Contact: jakubwosiak@gmail.com
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#include "numbertype.h"

qreal NumberType::getValue() const
{
    return value;
}

ContinuousDataType *NumberType::fromReal(qreal value) const
{
    return new NumberType(value);
}

NumberType::NumberType()
{

}

NumberType::NumberType(qreal value)
    : value(value)
{

}

QString NumberType::toString() const
{
    return format(value);
}

qreal NumberType::toReal() const
{
    return value;
}

QString NumberType::format(qreal value)
{
    return QString::number(value);
}

QString NumberType::intFormat(qreal value)
{
    return QString::number(int(value));
}
