How to compile and run:

1. Install PostgreSQL 10.
2. Create postgres user 'analyst' with password 'analyst1' and database 'enduhub_names' ('analyst' must be owner of 'enduhub_names'), or use an existing postgres user and/or database (but object "database" in UtilitiesModule/Utilities/config.json must be properly changed).
3. Import database from dump (link at the bottom).
4. Install QT 5.10 with charts module
5. Import project to Qt Creator (open DataAnalyst.pro)
6. Select Desktop Qt 5.10 GCC kit
7. Build and run project


Controls:

- double right-click on point/box/bar to show info
- scroll on chart to rescale
- drag scaled chart to navigate through it
- double-click on selected record (which is currently drawn) to deselect

Data Analyst window can be resized.

Program, which downloads new data from enduhub.com: https://bitbucket.org/jwosiak/recordsdownloader


Dump link:

- version 1: https://www.dropbox.com/s/ol169wxkdh3xjmt/enduhub_names.psql?dl=0
- version 2: https://www.dropbox.com/s/rn2r7xpjd6njuaz/baseRunnersDB.psql?dl=0
- version 3: https://www.dropbox.com/s/gcok9e1at6rwqx4/runners.psql?dl=0
- version 4(newest): https://www.dropbox.com/s/4retcc8yboygong/enduhub_runners.psql?dl=0
