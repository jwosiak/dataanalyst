ALTER TABLE records DROP COLUMN place_category;
ALTER TABLE records DROP COLUMN best_place;
CREATE TABLE temp (LIKE records);

INSERT INTO temp SELECT DISTINCT * FROM records 
WHERE distance IN (5000, 10000, 21097, 42195) 
    AND result IS NOT NULL
    AND LENGTH(result) >= 7
    AND result != '00:00:00'
    AND (male != 0 OR female != 0)
    AND place > 0;

TRUNCATE TABLE records;
INSERT INTO records SELECT * FROM temp;
DROP TABLE temp;


CREATE SEQUENCE IF NOT EXISTS person_id_sequence;
SELECT setval('person_id_sequence', 1);

CREATE TABLE IF NOT EXISTS person
(
    name TEXT NOT NULL,
    yob INT NOT NULL,
    id INT PRIMARY KEY,
    UNIQUE (name, yob)
);

CREATE SEQUENCE IF NOT EXISTS event_id_sequence;
SELECT setval('event_id_sequence', 1);

CREATE TABLE IF NOT EXISTS event
(
    id INT PRIMARY KEY,
    name TEXT NOT NULL,
    event_date DATE NOT NULL,
    distance REAL NOT NULL,
    registered_participants INT NOT NULL,
    max_general_place INT NOT NULL,
    UNIQUE (name, event_date, distance)
);

CREATE TABLE IF NOT EXISTS run
(
    person_id INT REFERENCES person(ID),
    event_id INT REFERENCES event(ID),
    nr INT NOT NULL,
    result INTERVAL NOT NULL,
    netto INTERVAL NOT NULL,
    male_place INT NOT NULL,
    female_place INT NOT NULL,
    general_place INT NOT NULL
);

INSERT INTO event (SELECT nextval('event_id_sequence'), event_name,
    CAST(event_date AS DATE), distance, COUNT(*), MAX(place)
FROM records GROUP BY event_name, event_date, distance
HAVING MAX(place) > 0 AND CAST(COUNT(*) AS real) / CAST(MAX(place) AS real) > 0.5
    AND COUNT(*) > 40)
ON CONFLICT DO NOTHING;

INSERT INTO person SELECT r.name, r.yob, nextval('event_id_sequence')
FROM records r JOIN event e 
    ON (r.event_name = e.name AND r.distance = e.distance
        AND CAST(r.event_date AS date) = e.event_date)
ON CONFLICT DO NOTHING;

CREATE OR REPLACE FUNCTION parseTime(str TEXT) RETURNS INTERVAL AS $$
    BEGIN
        IF str LIKE ('%_:%_:%_')
        THEN
            RETURN CAST(str AS INTERVAL);
        ELSE
            RETURN INTERVAL '0';
        END IF;
    END;
$$ LANGUAGE plpgsql;

INSERT INTO run 
    SELECT p.id, e.id, nr, parseTime(result), parseTime(netto), male, female, place
    FROM records r JOIN person p ON ((r.name, r.yob) = (p.name, p.yob))
        JOIN event e ON ((r.event_name, CAST(r.event_date AS DATE), r.distance) 
            = (e.name, e.event_date, e.distance))
ON CONFLICT DO NOTHING;

UPDATE run SET netto = result WHERE netto = (INTERVAL '0');

DROP FUNCTION parseTime(TEXT);

ALTER TABLE person DROP CONSTRAINT person_name_yob_key; --trzeba wyrzucić unikalność osób, ponieważ poniższe zapytania doprowadzą do konfliktów
UPDATE person SET name = upper(name) ;
UPDATE person SET yob = 1900 + yob WHERE yob BETWEEN 20 AND 99;
UPDATE person SET yob = 1000 + yob WHERE yob BETWEEN 920 AND 999;


CREATE TABLE personsSimilarities
(
    person_id INT,
    similar_to_person_id INT,
    person_name TEXT
);


CREATE INDEX IF NOT EXISTS person_name_yob_index ON person(name, yob);
CREATE INDEX IF NOT EXISTS personsSimilarities_id_index ON personsSimilarities(person_id);
CREATE INDEX IF NOT EXISTS personsSimilarities_id2_index ON personsSimilarities(similar_to_person_id);


WITH sp AS (
    SELECT id, yob, name, (n[2] || ' ' || n[1]) AS "revname" FROM (
        SELECT id, yob, name, regexp_split_to_array(name, ' ') AS "n" FROM person) q
) 
INSERT INTO personsSimilarities
SELECT DISTINCT LEAST(p.id, s1.id), GREATEST(p.id, s1.id), p.name FROM person p JOIN sp s1
    ON (
        p.id <> s1.id AND 
        ((p.name, p.yob) = (s1.name, s1.yob)
        OR (p.name, p.yob) = (s1.revname, s1.yob))
    )
ON CONFLICT DO NOTHING;

CREATE OR REPLACE FUNCTION fusePersons(id1 INT, id2 INT) RETURNS VOID AS $$
    BEGIN 
        DELETE FROM personsSimilarities WHERE person_id = id2 OR similar_to_person_id = id2;
        UPDATE run SET person_id = id1 WHERE person_id = id2;
        RETURN;
    END;
$$ LANGUAGE plpgsql;


CREATE INDEX IF NOT EXISTS run_person_id_index ON run(person_id);

SELECT DISTINCT 1 FROM (SELECT fusePersons(person_id, similar_to_person_id) FROM personsSimilarities) q;


WITH person_with_run AS (
    SELECT id FROM person JOIN run ON (id = person_id)
)
DELETE FROM person p WHERE NOT EXISTS (SELECT * FROM person_with_run pwr WHERE p.id = pwr.id);


ALTER TABLE person ADD CONSTRAINT person_name_yob_key UNIQUE (name, yob);
ALTER TABLE event ADD CONSTRAINT event_name_date_dist_key UNIQUE (name, event_date, distance);

CREATE TABLE run_temp (LIKE run);
ALTER TABLE run_temp ADD CONSTRAINT temp_run_personid_eventid_key UNIQUE (person_id, event_id);
INSERT INTO run_temp SELECT DISTINCT * FROM run ON CONFLICT DO NOTHING;
TRUNCATE TABLE run;
INSERT INTO run SELECT * FROM run_temp;
DROP TABLE run_temp CASCADE;
ALTER TABLE run add CONSTRAINT run_personid_eventid_key UNIQUE (person_id, event_id);

DROP INDEX person_name_yob_index;
DROP INDEX personsSimilarities_id_index;
DROP INDEX personsSimilarities_id2_index;
DROP INDEX run_person_id_index;
DROP TABLE personsSimilarities;

DROP FUNCTION fusePersons(id1 INT, id2 INT);

DROP SEQUENCE person_id_sequence;
DROP SEQUENCE event_id_sequence;


ALTER TABLE person ADD COLUMN runs INT DEFAULT 0 NOT NULL;
UPDATE person p0 SET runs = q."c"
FROM 
    (SELECT p.id, COUNT(*) AS "c"
    FROM person p JOIN run r ON (p.id = r.person_id)
    GROUP BY p.id) q
WHERE p0.id = q.id
;

ALTER TABLE person ADD COLUMN male BOOLEAN DEFAULT FALSE NOT NULL;
UPDATE person SET male = (SELECT COUNT(*) FROM run WHERE person_id = id AND male_place != 0) >= (SELECT COUNT(*) FROM run WHERE person_id = id AND female_place != 0);

UPDATE run r0 SET male_place = CASE WHEN male THEN male_place ELSE 0 END, female_place = CASE WHEN male THEN 0 ELSE female_place END
FROM 
    (SELECT r.person_id, r.event_id, p.male FROM run r JOIN person p ON (r.person_id = p.id)) q
WHERE (r0.person_id, r0.event_id) = (q.person_id, q.event_id);





ALTER TABLE run ADD COLUMN top_highscore_netto_ratio REAL DEFAULT 0 NOT NULL;
UPDATE run r0 SET top_highscore_netto_ratio = CASE q.male 
    WHEN true THEN CASE distance
        WHEN 5000 THEN 779.5
        WHEN 10000 THEN 1604.0
        WHEN 21097 THEN 3503.0
        ELSE 7377.0
        END
    ELSE CASE distance
        WHEN 5000 THEN 872.0
        WHEN 10000 THEN 1783.0
        WHEN 21097 THEN 3891.0
        ELSE 8125.0
        END
    END / q.s
FROM 
    (SELECT r.person_id, r.event_id, distance, p.male, (EXTRACT(HOUR FROM netto) * 3600 + EXTRACT(MINUTE FROM netto) * 60 + EXTRACT(SECOND FROM netto)) AS "s"
    FROM run r JOIN event e ON (r.event_id = e.id) JOIN person p ON (r.person_id = p.id)) q
WHERE (r0.person_id, r0.event_id) = (q.person_id, q.event_id) AND q.s != 0;


ALTER TABLE event ADD COLUMN max_male_place INT DEFAULT 0 NOT NULL;
ALTER TABLE event ADD COLUMN max_female_place INT DEFAULT 0 NOT NULL;
UPDATE event e0 SET max_male_place = q.m, max_female_place = q.f
FROM 
    (SELECT e.id, MAX(r.male_place) AS "m", MAX(r.female_place) AS "f" 
    FROM event e JOIN run r ON (e.id = r.event_id) 
    GROUP BY e.id) q
WHERE e0.id = q.id;
